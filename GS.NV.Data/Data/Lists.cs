/************************************************************************
**	ClassName	: 	List
**	Author		:	HoangLeAnhTuan
**	Company		:	GS
**	Date		:	31-10-2009 10:04 PM
************************************************************************/

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using GS.Data;

namespace GS.NV.Data
{
	#region List
	/// <summary>
	/// This object represents the properties and methods of a List.
	/// </summary>
	public class Lists : ObjectBase 
	{	
		public Lists()
		{
		}
		
		public Lists(IDataReader reader)
		{
			this.FromDataReader(reader);
		}
		
		public Lists(DataRow row)
		{
			this.FromDataRow(row);
		}
		
		public override void CopyFrom(object obj)
        {
            base.CopyFrom(obj);
			
			rowguid = (obj as Lists).rowguid;
			listType = (obj as Lists).listType;
			listCode = (obj as Lists).listCode;
			listName = (obj as Lists).listName;
			note = (obj as Lists).note;
			isSystem = (obj as Lists).isSystem;
        }
		
		public override void FromDataReader(IDataReader reader)
		{
			base.FromDataReader(reader);
			if (reader != null && !reader.IsClosed)
			{						
				if (!isNull("rowguid",reader)) rowguid = reader.GetGuid(reader.GetOrdinal("rowguid"));
				if (!isNull("ListType",reader)) listType = reader.GetString(reader.GetOrdinal("ListType"));
				if (!isNull("ListCode",reader)) listCode = reader.GetString(reader.GetOrdinal("ListCode"));
				if (!isNull("ListName",reader)) listName = reader.GetString(reader.GetOrdinal("ListName"));
				if (!isNull("Note",reader)) note = reader.GetString(reader.GetOrdinal("Note"));
				if (!isNull("IsSystem",reader)) isSystem = reader.GetBoolean(reader.GetOrdinal("IsSystem"));
			}
		}
		
		public override void FromDataRow(DataRow row)
		{
			base.FromDataRow(row);
			
			if (!row.IsNull("rowguid")) rowguid = (Guid)row["rowguid"];
			if (!row.IsNull("ListType")) listType = (string)row["ListType"];
			if (!row.IsNull("ListCode")) listCode = (string)row["ListCode"];
			if (!row.IsNull("ListName")) listName = (string)row["ListName"];
			if (!row.IsNull("Note")) note = (string)row["Note"];
			if (!row.IsNull("IsSystem")) isSystem = (bool)row["IsSystem"];
		}
		
		#region Public Properties	
		
		private Guid rowguid = Guid.Empty;
		/// <summary>
		/// Gets or sets the value of rowguid
		/// </summary>
		public Guid Rowguid
		{
			get {return rowguid;}
			set {rowguid = value;}
		}

		private string listType = String.Empty;
		/// <summary>
		/// Gets or sets the value of ListType
		/// </summary>
		public string ListType
		{
			get {return listType;}
			set {listType = value;}
		}

		private string listCode = String.Empty;
		/// <summary>
		/// Gets or sets the value of ListCode
		/// </summary>
		public string ListCode
		{
			get {return listCode;}
			set {listCode = value;}
		}

		private string listName = String.Empty;
		/// <summary>
		/// Gets or sets the value of ListName
		/// </summary>
		public string ListName
		{
			get {return listName;}
			set {listName = value;}
		}

		private string note = String.Empty;
		/// <summary>
		/// Gets or sets the value of Note
		/// </summary>
		public string Note
		{
			get {return note;}
			set {note = value;}
		}

		private bool isSystem;
		/// <summary>
		/// Gets or sets the value of IsSystem
		/// </summary>
		public bool IsSystem
		{
			get {return isSystem;}
			set {isSystem = value;}
		}
		#endregion

        #region Lists
        #endregion
    }
	#endregion
}	

