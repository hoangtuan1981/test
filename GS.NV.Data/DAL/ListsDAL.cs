/************************************************************************
**	ClassName	: 	ListDAL
**	Author		:	HoangLeAnhTuan
**	Company		:	GS
**	Date		:	31-10-2009 10:15 PM
************************************************************************/

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using GS.Data;

namespace GS.NV.Data
{
	#region ListsDAL
	/// <summary>
	/// This object represents the properties and methods of a Data Access Layer of List.
	/// </summary>
	public class ListsDAL : BaseDAL<Lists>
	{
		public ListsDAL()
		{
		}
        public ListsDAL(DBHelper dbHelper): base(dbHelper)
		{
			
		}
		#region Stored procedure wrappers
		/// <summary>
		/// Inserts an object into database by calling Insert StoredProcedure
		/// </summary>
		public override int Insert(Lists t)
		{
			int iError = 0;
            bool alreadyOpen = false;
            try
            {
                if (db.State != System.Data.ConnectionState.Open)
                    db.Open();
                else
                    alreadyOpen = true;
                DbCommand cmd = db.CreateCommand();
                cmd.CommandText = "usp_List_Insert";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				
                cmd.Parameters.Add(db.CreateParameter("@rowguid",System.Data.DbType.Guid, 16, t.Rowguid));
                cmd.Parameters.Add(db.CreateParameter("@ListType",System.Data.DbType.AnsiString, 20, t.ListType));
                cmd.Parameters.Add(db.CreateParameter("@ListCode",System.Data.DbType.AnsiString, 20, t.ListCode));
                cmd.Parameters.Add(db.CreateParameter("@ListName",System.Data.DbType.String, 200, t.ListName));
                cmd.Parameters.Add(db.CreateParameter("@Note",System.Data.DbType.String, 50, t.Note));
                cmd.Parameters.Add(db.CreateParameter("@IsSystem",System.Data.DbType.Boolean, 1, t.IsSystem));
				
                cmd.Parameters.Add(db.CreateParameter("@iError", System.Data.DbType.Int32, 4, 0, System.Data.ParameterDirection.Output));
                
				iError = db.ExecuteNonQuery(cmd);
				//if (iError == 0)
	            iError = (int)cmd.Parameters["@iError"].Value;
                if (iError == 0)
                {
                }
            }
            catch (Exception excp)
            {
                iError = -1000;
                Write2Log.WriteLogs("ListDAL", "Insert(List t)", excp.Message);
            }
            finally
            {
                if (!alreadyOpen)
                    db.Close();
            }
            return iError;
		}
		/// <summary>
		/// Updates an existing object in database by calling Update StoredProcedure
		/// </summary>
		public override int Update(Lists t)
		{
			int iError = 0;
            bool alreadyOpen = false;
            try
            {
                if (db.State != System.Data.ConnectionState.Open)
                    db.Open();
                else
                    alreadyOpen = true;
                DbCommand cmd = db.CreateCommand();
                cmd.CommandText = "usp_List_Update";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Parameters.Add(db.CreateParameter("@rowguid",System.Data.DbType.Guid, 16, t.Rowguid));
                cmd.Parameters.Add(db.CreateParameter("@ListType",System.Data.DbType.AnsiString, 20, t.ListType));
                cmd.Parameters.Add(db.CreateParameter("@ListCode",System.Data.DbType.AnsiString, 20, t.ListCode));
                cmd.Parameters.Add(db.CreateParameter("@ListName", System.Data.DbType.String, 200, t.ListName));
                cmd.Parameters.Add(db.CreateParameter("@Note",System.Data.DbType.String, 50, t.Note));
                cmd.Parameters.Add(db.CreateParameter("@IsSystem",System.Data.DbType.Boolean, 1, t.IsSystem));
				
                cmd.Parameters.Add(db.CreateParameter("@iError", System.Data.DbType.Int32, 4, 0, System.Data.ParameterDirection.Output));
                
				iError = db.ExecuteNonQuery(cmd);
				//if (iError == 0)
	            iError = (int)cmd.Parameters["@iError"].Value;
            }
            catch (Exception excp)
            {
                iError = -1000;
                Write2Log.WriteLogs("ListDAL", "Update(List t)", excp.Message);
            }
            finally
            {
                if (!alreadyOpen)
                    db.Close();
            }
            return iError;
		}
		
		/// <summary>
		/// Deletes an object from database by calling Delete StoredProcedure
		/// </summary>
		public override int Delete(Lists t)
		{
			           
            return this.Delete( t.Rowguid);
		}
		
		/// <summary>
		/// Deletes an object from database by calling Delete StoredProcedure
		/// </summary>		
		public int Delete(Guid rowguid)
		{
			int iError = 0;
            bool alreadyOpen = false;			
            try
            {
                if (db.State != System.Data.ConnectionState.Open)
                    db.Open();
                else
                    alreadyOpen = true;
                DbCommand cmd = db.CreateCommand();
                cmd.CommandText = "usp_List_Delete";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
				cmd.Parameters.Add(db.CreateParameter("@rowguid", System.Data.DbType.Guid , 16, rowguid));
                cmd.Parameters.Add(db.CreateParameter("@iError", System.Data.DbType.Int32, 4, 0, System.Data.ParameterDirection.Output));

                iError = db.ExecuteNonQuery(cmd);
				//if (iError == 0)
                	iError = (int)cmd.Parameters["@iError"].Value;
            }
            catch (Exception excp)
            {
                iError = -1000;
                Write2Log.WriteLogs("ListDAL", "Delete(List t)", excp.Message);
            }
            finally
            {
                if (!alreadyOpen)
                    db.Close();
            }
            return iError;
		}

        /// <summary>
        /// Returns an object from database by calling Select StoredProcedure
        /// </summary>		
        public Lists GetByID(Guid rowguid)
        {
            bool alreadyOpen = false;
            Lists obj = null;
            try
            {
                DbDataReader reader = null;
                if (db.State != System.Data.ConnectionState.Open)
                    db.Open();
                else
                    alreadyOpen = true;
                DbCommand cmd = db.CreateCommand();
                cmd.CommandText = "usp_Lists_Select";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(db.CreateParameter("@rowguid", System.Data.DbType.Guid, 16, rowguid));

                cmd.Parameters.Add(db.CreateParameter("@iError", System.Data.DbType.Int32, 4, 0, System.Data.ParameterDirection.Output));

                reader = db.ExecuteReader(cmd);
                if (reader.Read())
                    obj = new Lists(reader);
            }
            catch (Exception excp)
            {
                Write2Log.WriteLogs("ListDAL", "GetByID(Guid rowguid)", excp.Message);
            }
            finally
            {
                if (!alreadyOpen)
                    db.Close();
            }
            return obj;
        }	
		
		#endregion
		#region private methods
		
        protected override void SetValues()
        {
            _spSelectAll = "usp_List_SelectAll";
			_spSelectDynamic = "usp_List_SelectDynamic";
            _spDeleteAll = "usp_List_DeleteAll";            
			_spDeleteDynamic = "usp_List_DeleteDynamic";
        }
		#endregion
    }
	#endregion
}

