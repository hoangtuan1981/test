using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using GS.Data;

namespace GS.NV.Data
{
    #region ListsBLL
    /// <summary>
    /// This object represents the properties and methods of a Business Layer of Lists.
    /// </summary>
    public class ListsBLL : IBusiness
    {
        private ListsDAL dal = new ListsDAL();
        public ListsBLL()
		{
        }
        #region Stored procedure wrappers

        /// <summary>
        /// Gets all objects 
        /// </summary>
        public ListBase<Lists> GetAll()
        {
            return dal.GetObjectAll();
        }

        /// <summary>
        /// Gets all objects 
        /// </summary>
        public ListBase<Lists> GetDynamic(string whereCondition, string orderExpression)
        {
            return dal.GetObjectDynamic(whereCondition, orderExpression);
        }

        /// <summary>
        /// Inserts an object into database by calling Insert StoredProcedure
        /// </summary>
        public int Insert(Lists t)
        {
            return dal.Insert(t);
        }

        /// <summary>
        /// Delete all rows 
        /// </summary>
        public int DeleteAll()
        {
            return dal.DeleteAll();
        }

        /// <summary>
        /// Delete rows by dynamic criteria
        /// </summary>
        public int DeleteDynamic(string whereCondidion)
        {
            return dal.DeleteDynamic(whereCondidion);
        }

        /// <summary>
        /// Updates an existing object in database 
        /// </summary>
        public int Update(Lists t)
        {
            return dal.Update(t);
        }

        /// <summary>
        /// Returns an object by ID
        /// </summary>		
        public Lists GetByID(Guid rowguid)
        {
            return dal.GetByID(rowguid);
        }

        /// <summary>
        /// Deletes an object from database by Id
        /// </summary>		
        public int Delete(Guid rowguid)
        {
            return dal.Delete(rowguid);
        }

        /// <summary>
        /// Deletes an object from database 
        /// </summary>		
        public int Delete(Lists t)
        {
            return dal.Delete(t.Rowguid);
        }

        #endregion

        #region IBusiness Members

        public int Insert(object obj)
        {
            return this.Insert(obj as Lists);
        }

        public int Update(object obj)
        {
            return this.Update(obj as Lists);
        }

        public int Delete(object obj)
        {
            return this.Delete(obj as Lists);
        }

        #endregion

        #region Old Code

        ///// <summary>
        ///// Gets all objects 
        ///// </summary>
        //public ListBase<Lists> WarrantyArticleName()
        //{
        //    string whereCondition = "ListType = 'ARP'";
        //    string orderBy = "ListName";
        //    return GetDynamic(whereCondition, orderBy);
        //}

        #endregion Old Code

    }
    #endregion
}
