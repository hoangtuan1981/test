using System;
using System.Collections.Generic;
using System.Text;

using VNS.MainApplication;
namespace VNS.UserManagement.GUI
{
    public class ModuleUser : MainModule
    {
        public ModuleUser()
            : base("ModuleUser", "Quản lý người dùng")
        {

            LoadInfoModule();

            this.NavBarGroupInfo.NewChildGroup("List", "Danh sách");
            this.NavBarGroupInfo.NewChildNavBarItem(typeof(UCListUser), "ListUsers", "Người dùng");

            this.NavBarGroupInfo.NewChildGroup("NewTask", "Tạo mới");
            this.NavBarGroupInfo.NewChildNavBarItem("User", "Người dùng", typeof(FrmEditUser));
        }
    }
}
