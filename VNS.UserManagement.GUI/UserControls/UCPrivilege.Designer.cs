namespace GS.UserManagement.GUI.UserControls
{
    partial class UCPrivilege
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMemberID = new DevExpress.XtraEditors.LabelControl();
            this.txtMemberID = new DevExpress.XtraEditors.TextEdit();
            this.lblMemberName = new DevExpress.XtraEditors.LabelControl();
            this.txtMemberName = new DevExpress.XtraEditors.TextEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFunctionID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllowView = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllowAdd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllowEdit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAllowDelete = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repTextHide = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.txtMemberID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMemberName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTextHide)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblMemberID
            // 
            this.lblMemberID.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblMemberID.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMemberID.Appearance.Options.UseFont = true;
            this.lblMemberID.Location = new System.Drawing.Point(32, 5);
            this.lblMemberID.Name = "lblMemberID";
            this.lblMemberID.Size = new System.Drawing.Size(15, 14);
            this.lblMemberID.TabIndex = 0;
            this.lblMemberID.Text = "Mã";
            // 
            // txtMemberID
            // 
            this.txtMemberID.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtMemberID.Location = new System.Drawing.Point(53, 3);
            this.txtMemberID.Name = "txtMemberID";
            this.txtMemberID.Size = new System.Drawing.Size(149, 20);
            this.txtMemberID.TabIndex = 1;
            // 
            // lblMemberName
            // 
            this.lblMemberName.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblMemberName.Appearance.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMemberName.Appearance.Options.UseFont = true;
            this.lblMemberName.Location = new System.Drawing.Point(248, 5);
            this.lblMemberName.Name = "lblMemberName";
            this.lblMemberName.Size = new System.Drawing.Size(22, 14);
            this.lblMemberName.TabIndex = 2;
            this.lblMemberName.Text = "Tên";
            // 
            // txtMemberName
            // 
            this.txtMemberName.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.txtMemberName.Location = new System.Drawing.Point(276, 3);
            this.txtMemberName.Name = "txtMemberName";
            this.txtMemberName.Size = new System.Drawing.Size(173, 20);
            this.txtMemberName.TabIndex = 3;
            // 
            // gridControl1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.gridControl1, 6);
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.EmbeddedNavigator.Name = "";
            this.gridControl1.Location = new System.Drawing.Point(3, 28);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repTextHide});
            this.gridControl1.Size = new System.Drawing.Size(582, 324);
            this.gridControl1.TabIndex = 4;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.gridView2});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFunctionID,
            this.colGroupName,
            this.colAllowView,
            this.colAllowAdd,
            this.colAllowEdit,
            this.colAllowDelete});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AutoExpandAllGroups = true;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colGroupName, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            // 
            // colFunctionID
            // 
            this.colFunctionID.Caption = "Chức năng";
            this.colFunctionID.FieldName = "Description";
            this.colFunctionID.Name = "colFunctionID";
            this.colFunctionID.Visible = true;
            this.colFunctionID.VisibleIndex = 0;
            this.colFunctionID.Width = 150;
            // 
            // colGroupName
            // 
            this.colGroupName.Caption = "Nhóm";
            this.colGroupName.FieldName = "GroupName";
            this.colGroupName.Name = "colGroupName";
            this.colGroupName.Visible = true;
            this.colGroupName.VisibleIndex = 1;
            this.colGroupName.Width = 81;
            // 
            // colAllowView
            // 
            this.colAllowView.Caption = "Truy cập";
            this.colAllowView.FieldName = "AllowView";
            this.colAllowView.Name = "colAllowView";
            this.colAllowView.Visible = true;
            this.colAllowView.VisibleIndex = 1;
            this.colAllowView.Width = 81;
            // 
            // colAllowAdd
            // 
            this.colAllowAdd.Caption = "Thêm";
            this.colAllowAdd.FieldName = "AllowAdd";
            this.colAllowAdd.Name = "colAllowAdd";
            this.colAllowAdd.Visible = true;
            this.colAllowAdd.VisibleIndex = 2;
            this.colAllowAdd.Width = 81;
            // 
            // colAllowEdit
            // 
            this.colAllowEdit.Caption = "Sửa";
            this.colAllowEdit.FieldName = "AllowEdit";
            this.colAllowEdit.Name = "colAllowEdit";
            this.colAllowEdit.Visible = true;
            this.colAllowEdit.VisibleIndex = 3;
            this.colAllowEdit.Width = 81;
            // 
            // colAllowDelete
            // 
            this.colAllowDelete.Caption = "Xóa";
            this.colAllowDelete.FieldName = "AllowDelete";
            this.colAllowDelete.Name = "colAllowDelete";
            this.colAllowDelete.Visible = true;
            this.colAllowDelete.VisibleIndex = 4;
            this.colAllowDelete.Width = 87;
            // 
            // repTextHide
            // 
            this.repTextHide.AllowFocused = false;
            this.repTextHide.AutoHeight = false;
            this.repTextHide.Name = "repTextHide";
            this.repTextHide.PasswordChar = ' ';
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 159F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 179F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.gridControl1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblMemberName, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtMemberName, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblMemberID, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtMemberID, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(588, 355);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // UCPrivilege
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UCPrivilege";
            this.Size = new System.Drawing.Size(588, 355);
            ((System.ComponentModel.ISupportInitialize)(this.txtMemberID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMemberName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repTextHide)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.LabelControl lblMemberID;
        private DevExpress.XtraEditors.TextEdit txtMemberID;
        private DevExpress.XtraEditors.LabelControl lblMemberName;
        private DevExpress.XtraEditors.TextEdit txtMemberName;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colFunctionID;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupName;
        private DevExpress.XtraGrid.Columns.GridColumn colAllowView;
        private DevExpress.XtraGrid.Columns.GridColumn colAllowAdd;
        private DevExpress.XtraGrid.Columns.GridColumn colAllowEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colAllowDelete;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repTextHide;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}
