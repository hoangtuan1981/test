namespace GS.UserManagement.GUI
{
    partial class UCEditUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanelUser = new System.Windows.Forms.TableLayoutPanel();
            this.txtMemberID = new DevExpress.XtraEditors.TextEdit();
            this.txtMemberName = new DevExpress.XtraEditors.TextEdit();
            this.txtDescription = new DevExpress.XtraEditors.MemoEdit();
            this.chkIsAdmin = new DevExpress.XtraEditors.CheckEdit();
            this.lblLoginName = new DevExpress.XtraEditors.LabelControl();
            this.lblMemberName = new DevExpress.XtraEditors.LabelControl();
            this.lblDescription = new DevExpress.XtraEditors.LabelControl();
            this.tableLayoutPanelUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMemberID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMemberName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsAdmin.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanelUser
            // 
            this.tableLayoutPanelUser.ColumnCount = 2;
            this.tableLayoutPanelUser.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanelUser.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelUser.Controls.Add(this.txtMemberID, 1, 0);
            this.tableLayoutPanelUser.Controls.Add(this.txtMemberName, 1, 1);
            this.tableLayoutPanelUser.Controls.Add(this.txtDescription, 1, 3);
            this.tableLayoutPanelUser.Controls.Add(this.chkIsAdmin, 1, 2);
            this.tableLayoutPanelUser.Controls.Add(this.lblLoginName, 0, 0);
            this.tableLayoutPanelUser.Controls.Add(this.lblMemberName, 0, 1);
            this.tableLayoutPanelUser.Controls.Add(this.lblDescription, 0, 3);
            this.tableLayoutPanelUser.Location = new System.Drawing.Point(2, 4);
            this.tableLayoutPanelUser.Name = "tableLayoutPanelUser";
            this.tableLayoutPanelUser.RowCount = 4;
            this.tableLayoutPanelUser.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanelUser.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanelUser.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanelUser.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelUser.Size = new System.Drawing.Size(344, 143);
            this.tableLayoutPanelUser.TabIndex = 0;
            // 
            // txtMemberID
            // 
            this.txtMemberID.EnterMoveNextControl = true;
            this.txtMemberID.Location = new System.Drawing.Point(103, 3);
            this.txtMemberID.Name = "txtMemberID";
            this.txtMemberID.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMemberID.Size = new System.Drawing.Size(100, 20);
            this.txtMemberID.TabIndex = 1;
            // 
            // txtMemberName
            // 
            this.txtMemberName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMemberName.EnterMoveNextControl = true;
            this.txtMemberName.Location = new System.Drawing.Point(103, 28);
            this.txtMemberName.Name = "txtMemberName";
            this.txtMemberName.Size = new System.Drawing.Size(238, 20);
            this.txtMemberName.TabIndex = 3;
            // 
            // txtDescription
            // 
            this.txtDescription.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDescription.EnterMoveNextControl = true;
            this.txtDescription.Location = new System.Drawing.Point(103, 78);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(238, 62);
            this.txtDescription.TabIndex = 5;
            // 
            // chkIsAdmin
            // 
            this.chkIsAdmin.Location = new System.Drawing.Point(103, 53);
            this.chkIsAdmin.Name = "chkIsAdmin";
            this.chkIsAdmin.Properties.Caption = "Quản trị";
            this.chkIsAdmin.Size = new System.Drawing.Size(75, 18);
            this.chkIsAdmin.TabIndex = 6;
            this.chkIsAdmin.ToolTip = "管理";
            // 
            // lblLoginName
            // 
            this.lblLoginName.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblLoginName.Location = new System.Drawing.Point(23, 3);
            this.lblLoginName.Name = "lblLoginName";
            this.lblLoginName.Size = new System.Drawing.Size(74, 13);
            this.lblLoginName.TabIndex = 7;
            this.lblLoginName.Text = "Tên Đăng nhập";
            // 
            // lblMemberName
            // 
            this.lblMemberName.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblMemberName.Location = new System.Drawing.Point(22, 28);
            this.lblMemberName.Name = "lblMemberName";
            this.lblMemberName.Size = new System.Drawing.Size(75, 13);
            this.lblMemberName.TabIndex = 8;
            this.lblMemberName.Text = "Tên người dùng";
            this.lblMemberName.ToolTip = "名稱";
            // 
            // lblDescription
            // 
            this.lblDescription.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblDescription.Location = new System.Drawing.Point(62, 78);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(35, 13);
            this.lblDescription.TabIndex = 9;
            this.lblDescription.Tag = "備註";
            this.lblDescription.Text = "Ghi chú";
            // 
            // UCEditUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.Controls.Add(this.tableLayoutPanelUser);
            this.Name = "UCEditUser";
            this.Size = new System.Drawing.Size(349, 150);
            this.tableLayoutPanelUser.ResumeLayout(false);
            this.tableLayoutPanelUser.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMemberID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMemberName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescription.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkIsAdmin.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelUser;
        private DevExpress.XtraEditors.TextEdit txtMemberID;
        private DevExpress.XtraEditors.TextEdit txtMemberName;
        private DevExpress.XtraEditors.MemoEdit txtDescription;
        private DevExpress.XtraEditors.CheckEdit chkIsAdmin;
        private DevExpress.XtraEditors.LabelControl lblLoginName;
        private DevExpress.XtraEditors.LabelControl lblMemberName;
        private DevExpress.XtraEditors.LabelControl lblDescription;
    }
}
