using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GS.Data;
using GS.Windows;

namespace GS.UserManagement.GUI
{
    public partial class UCListUser : GS.Windows.Controls.UCListBase
    {
        public UCListUser()
        {
            InitializeComponent();
            this.Business = new UserBLL();
        }

        private void UCListUser_Load(object sender, EventArgs e)
        {
            this.DataSource = (this.Business as UserBLL).GetAllUser();
            this.FormOpenType = typeof(FrmEditUser);
        }
    }
}

