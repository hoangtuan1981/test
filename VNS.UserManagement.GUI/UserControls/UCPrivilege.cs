using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GS.Data;

namespace GS.UserManagement.GUI.UserControls
{
    public partial class UCPrivilege : GS.Windows.Controls.EditControlBase
    {
        public UCPrivilege()
        {
            InitializeComponent();
        }
        public override void ControlSettings()
        {
            base.ControlSettings();
            this.SetControlStatus(this.txtMemberID, enumControlStatus.IDData);
            this.SetControlStatus(this.txtMemberName, enumControlStatus.IDData);
            this.SetControlStatus(this.gridControl1, enumControlStatus.NormalData);

            this.SetControlDataBind(this.txtMemberID, "MemberID");
            this.SetControlDataBind(this.txtMemberName, "MemberName");
            this.SetControlDataBind(this.gridControl1, "ListMemberFunctionEdit");
        }

        private void gridView1_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            if (e.RowHandle >= 0)
            {
                if (!(gridView1.GetRow(e.RowHandle) as MemberFunctionEdit).CanAdd)
                {
                    if (e.Column.FieldName=="AllowAdd")
                        e.RepositoryItem = this.repTextHide;
                }
                if (!(gridView1.GetRow(e.RowHandle) as MemberFunctionEdit).CanEdit)
                {
                    if (e.Column.FieldName == "AllowEdit")
                        e.RepositoryItem = this.repTextHide;
                }
                if (!(gridView1.GetRow(e.RowHandle) as MemberFunctionEdit).CanDelete)
                {
                    if (e.Column.FieldName == "AllowDelete")
                        e.RepositoryItem = this.repTextHide;
                }
            }
        }

        protected override int ValidateData()
        {
            this.gridView1.CloseEditor();
            return base.ValidateData();
        }
        public override void RefreshControl()
        {
            base.RefreshControl();
            this.gridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.None;
        }
    }
}

