namespace GS.UserManagement.GUI
{
    partial class FormEditPrivilege
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ucPrivilege1 = new GS.UserManagement.GUI.UserControls.UCPrivilege();
            ((System.ComponentModel.ISupportInitialize)(this.bdSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.defaultBarAndDocking.Controller)).BeginInit();
            this.SuspendLayout();
            // 
            // defaultLookAndFeel
            // 
            this.defaultLookAndFeel.LookAndFeel.SkinName = "The Asphalt World";
            // 
            // defaultBarAndDocking
            // 
            this.defaultBarAndDocking.Controller.LookAndFeel.SkinName = "Stardust";
            this.defaultBarAndDocking.Controller.PaintStyleName = "Skin";
            // 
            // ucPrivilege1
            // 
            this.ucPrivilege1.Business = null;
            this.ucPrivilege1.DataSource = null;
            this.ucPrivilege1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucPrivilege1.FirstControl = null;
            this.ucPrivilege1.Location = new System.Drawing.Point(0, 67);
            this.ucPrivilege1.Margin = new System.Windows.Forms.Padding(0);
            this.ucPrivilege1.Name = "ucPrivilege1";
            this.ucPrivilege1.Size = new System.Drawing.Size(586, 306);
            this.ucPrivilege1.TabIndex = 10;
            // 
            // FormEditPrivilege
            // 
            this.AllowAddNew = false;
            this.AllowDelete = false;
            this.ClientSize = new System.Drawing.Size(586, 396);
            this.Controls.Add(this.ucPrivilege1);
            this.EditControl = this.ucPrivilege1;
            this.Location = new System.Drawing.Point(594, 430);
            this.Name = "FormEditPrivilege";
            this.Text = "Phân quyền sử dụng";
            this.Controls.SetChildIndex(this.ucPrivilege1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.bdSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.defaultBarAndDocking.Controller)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private GS.UserManagement.GUI.UserControls.UCPrivilege ucPrivilege1;
    }
}
