namespace GS.UserManagement.GUI
{
    partial class FrmEditUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmEditUser));
            this.ucEditUser1 = new GS.UserManagement.GUI.UCEditUser();
            this.cmdPrivilege = new DevExpress.XtraEditors.SimpleButton();
            this.btnSetPassword = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.bdSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.defaultBarAndDocking.Controller)).BeginInit();
            this.SuspendLayout();
            // 
            // defaultLookAndFeel
            // 
            this.defaultLookAndFeel.LookAndFeel.SkinName = "The Asphalt World";
            // 
            // defaultBarAndDocking
            // 
            this.defaultBarAndDocking.Controller.LookAndFeel.SkinName = "Stardust";
            this.defaultBarAndDocking.Controller.PaintStyleName = "Skin";
            // 
            // ucEditUser1
            // 
            this.ucEditUser1.Business = null;
            this.ucEditUser1.DataSource = null;
            this.ucEditUser1.FirstControl = null;
            this.ucEditUser1.Kethua = false;
            this.ucEditUser1.Location = new System.Drawing.Point(1, 39);
            this.ucEditUser1.Name = "ucEditUser1";
            this.ucEditUser1.Size = new System.Drawing.Size(349, 150);
            this.ucEditUser1.TabIndex = 10;
            // 
            // cmdPrivilege
            // 
            this.cmdPrivilege.Location = new System.Drawing.Point(232, 195);
            this.cmdPrivilege.Name = "cmdPrivilege";
            this.cmdPrivilege.Size = new System.Drawing.Size(109, 23);
            this.cmdPrivilege.TabIndex = 11;
            this.cmdPrivilege.Text = "Phân quyền truy cập";
            this.cmdPrivilege.ToolTip = "分權使用";
            this.cmdPrivilege.Click += new System.EventHandler(this.cmdPrivilege_Click);
            // 
            // btnSetPassword
            // 
            this.btnSetPassword.Location = new System.Drawing.Point(61, 195);
            this.btnSetPassword.Name = "btnSetPassword";
            this.btnSetPassword.Size = new System.Drawing.Size(100, 23);
            this.btnSetPassword.TabIndex = 12;
            this.btnSetPassword.Text = "Cài đặt mật khẩu";
            this.btnSetPassword.ToolTip = "密碼";
            this.btnSetPassword.Click += new System.EventHandler(this.btnSetPassword_Click);
            // 
            // FrmEditUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(394, 248);
            this.Controls.Add(this.btnSetPassword);
            this.Controls.Add(this.cmdPrivilege);
            this.Controls.Add(this.ucEditUser1);
            this.EditControl = this.ucEditUser1;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(410, 286);
            this.MinimumSize = new System.Drawing.Size(410, 286);
            this.Name = "FrmEditUser";
            this.ShowIcon = true;
            this.Text = "Thông tin người dùng";
            this.Controls.SetChildIndex(this.ucEditUser1, 0);
            this.Controls.SetChildIndex(this.cmdPrivilege, 0);
            this.Controls.SetChildIndex(this.btnSetPassword, 0);
            ((System.ComponentModel.ISupportInitialize)(this.bdSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.defaultBarAndDocking.Controller)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private UCEditUser ucEditUser1;
        private DevExpress.XtraEditors.SimpleButton cmdPrivilege;
        private DevExpress.XtraEditors.SimpleButton btnSetPassword;
    }
}
