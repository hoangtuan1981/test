using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;

using GS.Windows.Forms;
using GS.Data;

namespace GS.UserManagement.GUI
{
    public partial class FormLogin : FormBase
    {
        //public User DatasourceUser;
        protected User tempUser = null;
        protected DateTime tempWorkingDate = DateTime.Today;
        public FormLogin()
        {
            InitializeComponent();
        }

        private void OK_Click(object sender, EventArgs e)
        {
            int iLogin = this.DoLogin();
            if (iLogin == 0)
            {
                this.SetContext();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                this.DialogResult = DialogResult.None;
                MessageBox.Show(this.GetTextMessage("error"+iLogin.ToString(),"Error Login"));

                if (iLogin==-1)
                    this.txtPassword.Focus();
                if (iLogin == -2)
                    this.txtLoginName.Focus();
            }
            
        }

        protected virtual int DoLogin()
        {
            int iReturn = 0;
            tempUser = new UserBLL().GetByMemberID(txtLoginName.Text);
            //tempWorkingDate = (DateTime)this.txtWorkingDate.EditValue;
            if (tempUser != null)
            {
                if (tempUser.Password.Equals(txtPassword.Text))
                {
                    iReturn = 0;
                }
                else
                {
                    iReturn = -1;
                }
            }
            else
            {
                iReturn = -2;
            }
            return iReturn;
        }
        protected virtual void SetContext()
        {
            ContextBase.CurrentUser = tempUser;
            ContextBase.WorkingDate = tempWorkingDate;
        }
        private void Cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}