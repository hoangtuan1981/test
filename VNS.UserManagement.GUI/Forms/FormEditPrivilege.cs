using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GS.Data;

namespace GS.UserManagement.GUI
{
    public partial class FormEditPrivilege : GS.Windows.Forms.FormEditBase
    {
        public FormEditPrivilege()
        {
            InitializeComponent();
        }
        public FormEditPrivilege(Member m)
        {
            InitializeComponent();
            this.Business = new MemberFunctionBLL();
            m.ListMemberFunctionEdit = (this.Business as MemberFunctionBLL).GetListMemberFunctionEdit(m.MemberID);
            this.DataSource = m;
        }
    }
}

