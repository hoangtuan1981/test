using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using GS.Data;
using GS.Windows.Forms;

namespace GS.UserManagement.GUI
{
    public partial class FormSetPassword : FormBase
    {
        private User user;
        public FormSetPassword()
        {
            InitializeComponent();
        }
        public FormSetPassword(User pUser)
        {
            InitializeComponent();
            this.user = pUser;
        }
        private void btnOk_Click(object sender, EventArgs e)
        {
            string messageType, message;
            message = "Error Data";
            int Err;
            if (txtPassword.Text == txtConfirmPassword.Text)
            {
                messageType = "UPDATE";
                user.Password = txtPassword.Text;
                Err = new UserBLL().UpdatePassword(user);   //UpdatePassword(ContextBase.CurrentUser.LoginName, ContextBase.CurrentUser.Password);
            }
            else
            {
                messageType = "Confirm";
                MessageBox.Show(GetTextMessage(messageType, message));
                return;
            }

            if (Err != 0)
                MessageBox.Show(GetTextMessage(messageType + Err.ToString(), message));
            else
            {
                MessageBox.Show(GetTextMessage(messageType, message));
                this.Close();
            }
            this.Cursor = Cursors.Default;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtCofirmPassword_Enter(object sender, EventArgs e)
        {
            this.AcceptButton = btnOk;
        }
    }
}

