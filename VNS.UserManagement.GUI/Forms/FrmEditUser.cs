using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using GS.Data;

namespace GS.UserManagement.GUI
{
    public partial class FrmEditUser : GS.Windows.Forms.FormEditBase
    {
        public FrmEditUser()
        {
            InitializeComponent();
            this.DataSource = new ListBase<User>();
            this.Business = new UserBLL();
        }

        private void cmdPrivilege_Click(object sender, EventArgs e)
        {
            FormEditPrivilege f = new FormEditPrivilege(this.CurrentItem as Member);
            this.ShowChildForm(f);
        }

        public override void RefreshButtons(bool buttonOnly)
        {
            base.RefreshButtons(buttonOnly);
            if (this.EditMode == GS.Windows.FormEditMode.VIEW)
            {
                cmdPrivilege.Enabled = true;
                btnSetPassword.Enabled = true;
            }
            else
            {
                cmdPrivilege.Enabled = false;
                btnSetPassword.Enabled = false;
            }
        }

        private void btnSetPassword_Click(object sender, EventArgs e)
        {
            FormSetPassword f = new FormSetPassword(this.CurrentItem as User);
            this.ShowChildForm(f);
        }

    }
}

