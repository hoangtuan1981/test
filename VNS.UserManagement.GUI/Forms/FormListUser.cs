using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GS.Data;
using DevExpress.XtraEditors;

namespace GS.UserManagement.GUI
{
    public partial class FormListUser : GS.Windows.Forms.FormEditBase
    {
        public FormListUser()
        {
            InitializeComponent();
            this.FormEditType = typeof(FrmEditUser);
            this.Business = new UserBLL();
            this.DataSource = (this.Business as UserBLL).GetAllUser();
        }

    }
}

