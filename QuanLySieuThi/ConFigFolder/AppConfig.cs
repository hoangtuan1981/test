using System;
using System.Collections.Generic;
using System.Text;
using GS.Data;
namespace QLCC
{
    public class AppConfig:AppConfigBase
    {
        #region DateTime
        public static string CONFIG_DATETIME_FORMAT = "dd/MM/yy hh:mm tt";
        public static string CONFIG_DATETIME_FORMAT_STRING = "{0:" + CONFIG_DATETIME_FORMAT + "}";
        public static string CONFIG_DATETIME_MONTH_YEAR_FORMAT = "MM/yyyy";
        public static string CONFIG_DATETIME_MONTH_YEAR_FORMAT_STRING = "{0:" + CONFIG_DATETIME_MONTH_YEAR_FORMAT + "}";

        #endregion

        #region TaxRate
        public static string CONFIG_EXCHANGERATE_DISPLAY_FORMAT = "#,##0.##";
        public static string CONFIG_EXCHANGERATE_DISPLAY_FORMAT_STRING = "{0:" + CONFIG_EXCHANGERATE_DISPLAY_FORMAT + "}";

        public static string CONFIG_EXCHANGERATE_MASK_FORMAT = "#,###.##";
        public static string CONFIG_EXCHANGERATE_MASK_FORMAT_STRING = "{0:" + CONFIG_EXCHANGERATE_MASK_FORMAT + "}";
        #endregion

        #region Percent
        public static string CONFIG_PERCENT_DISPLAY_FORMAT = "##0%";
        public static string CONFIG_PERCENT_DISPLAY_FORMAT_STRING = "{0:" + CONFIG_PERCENT_DISPLAY_FORMAT + "}";

        public static string CONFIG_PERCENT_MASK_FORMAT = "###%";
        public static string CONFIG_PERCENT_MASK_FORMAT_STRING = "{0:" + CONFIG_PERCENT_MASK_FORMAT + "}";

        public static string CONFIG_PERCENT_REPORT_FORMAT = "##0%";
        public static string CONFIG_PERCENT_REPORT_FORMAT_STRING = "{0:" + CONFIG_PERCENT_REPORT_FORMAT + "}";
        #endregion

        #region GrossWeight
        public static string CONFIG_GROSSWEIGHT_KGFORMAT = "#,###,###.## Kgs";
        public static string CONFIG_GROSSWEIGHT_KGFORMAT_STRING = "{0:" + CONFIG_GROSSWEIGHT_KGFORMAT + "}";

        public static string CONFIG_GROSSWEIGHT_MASK_KGFORMAT = "#,###,###,###,###.## Kgs";
        public static string CONFIG_GROSSWEIGHT_MASK_KGFORMAT_STRING = "{0:" + CONFIG_GROSSWEIGHT_MASK_KGFORMAT + "}";

        public static string CONFIG_GROSSWEIGHT_MASK_FORMAT = "#,###,###,###,###.##";
        public static string CONFIG_GROSSWEIGHT_MASK_FORMAT_STRING = "{0:" + CONFIG_GROSSWEIGHT_MASK_FORMAT + "}";

        public static string CONFIG_GROSSWEIGHT_DISPLAY_FORMAT = "#,###,###.##";
        public static string CONFIG_GROSSWEIGHT_DISPLAY_FORMAT_STRING = "{0:" + CONFIG_GROSSWEIGHT_DISPLAY_FORMAT + "}";

        public static string CONFIG_GROSSWEIGHT_REPORT_FORMAT = "n2";
        public static string CONFIG_GROSSWEIGHT_REPORT_FORMAT_STRING = "{0:" + CONFIG_GROSSWEIGHT_REPORT_FORMAT + "}";
        #endregion

        #region Quantity
        public static string CONFIG_QUANTITY_PCSFORMAT = "#,###,###,### Pcs";
        public static string CONFIG_QUANTITY_PCSFORMAT_STRING = "{0:" + CONFIG_QUANTITY_PCSFORMAT + "}";

        public static string CONFIG_QUANTITY_MASK_PCSFORMAT = "#,###,###,###,###,### Pcs";
        public static string CONFIG_QUANTITY_MASK_PCSFORMAT_STRING = "{0:" + CONFIG_QUANTITY_MASK_PCSFORMAT + "}";

        public static string CONFIG_QUANTITY_DISPLAY_FORMAT = "#,###,###,###";
        public static string CONFIG_QUANTITY_DISPLAY_FORMAT_STRING = "{0:" + CONFIG_QUANTITY_DISPLAY_FORMAT + "}";

        public static string CONFIG_QUANTITY_MASK_FORMAT = "#,###,###,###,###";
        public static string CONFIG_QUANTITY_MASK_FORMAT_STRING = "{0:" + CONFIG_QUANTITY_MASK_FORMAT + "}";

        public static string CONFIG_QUANTITY_REPORT_FORMAT = "n0";
        public static string CONFIG_QUANTITY_REPORT_FORMAT_STRING = "{0:" + CONFIG_QUANTITY_REPORT_FORMAT + "}";

        #endregion

        #region RatePkg
        public static string CONFIG_RATEPKGFORMAT = "#,##0.##";
        public static string CONFIG_RATEPKGFORMAT_STRING = "{0:" + CONFIG_RATEPKGFORMAT + "}";

        public static string CONFIG_RATEPKG_MASK_FORMAT = "#,###,###,###.##";
        public static string CONFIG_RATEPKG_MASK_FORMAT_STRING = "{0:" + CONFIG_RATEPKG_MASK_FORMAT + "}";

        public static string CONFIG_RATEPKG_DISPLAY_FORMAT = "#,##0.##";
        public static string CONFIG_RATEPKG_DISPLAY_FORMAT_STRING = "{0:" + CONFIG_RATEPKG_DISPLAY_FORMAT + "}";
        #endregion

        #region PriceVN
        public static string CONFIG_PRICEVN_MASK_FORMAT = "#,###,###,###,###";
        public static string CONFIG_PRICEVN_MASK_FORMAT_STRING = "{0:" + CONFIG_PRICEVN_MASK_FORMAT + "}";

        public static string CONFIG_PRICEVN_DISPLAY_FORMAT = "#,###,###";
        public static string CONFIG_PRICEVN_DISPLAY_FORMAT_STRING = "{0:" + CONFIG_PRICEVN_DISPLAY_FORMAT + "}";

        public static string CONFIG_PRICEVN_REPORT_FORMAT = "n0";
        public static string CONFIG_PRICEVN_REPORT_FORMAT_STRING = "{0:" + CONFIG_PRICEVN_REPORT_FORMAT + "}";

        #endregion

        #region PriceOS
        public static string CONFIG_PRICEOS_MASK_FORMAT = "#,###,###,###,###.##";
        public static string CONFIG_PRICEOS_MASK_FORMAT_STRING = "{0:" + CONFIG_PRICEOS_MASK_FORMAT + "}";

        public static string CONFIG_PRICEOS_REPORT_FORMAT = "n2";
        public static string CONFIG_PRICEOS_REPORT_FORMAT_STRING = "{0:" + CONFIG_PRICEOS_REPORT_FORMAT + "}";

        public static string CONFIG_PRICEOS_DISPLAY_FORMAT = "#,###.##";
        public static string CONFIG_PRICEOS_DISPLAY_FORMAT_STRING = "{0:" + CONFIG_PRICEOS_DISPLAY_FORMAT + "}";
        #endregion

        #region AmountVN
        public static string CONFIG_AMOUNTVN_MASK_FORMAT = "###,###,###,###,###";
        public static string CONFIG_AMOUNTVN_MASK_FORMAT_STRING = "{0:" + CONFIG_AMOUNTVN_MASK_FORMAT + "}";

        public static string CONFIG_AMOUNTVN_DISPLAY_FORMAT = "###,###,###,###";
        public static string CONFIG_AMOUNTVN_DISPLAY_FORMAT_STRING = "{0:" + CONFIG_AMOUNTVN_DISPLAY_FORMAT + "}";

        public static string CONFIG_AMOUNTVN_REPORT_FORMAT = "n0";
        public static string CONFIG_AMOUNTVN_REPORT_FORMAT_STRING = "{0:" + CONFIG_AMOUNTVN_REPORT_FORMAT + "}";

        #endregion

        #region AmountOS
        public static string CONFIG_AMOUNTOS_MASK_FORMAT = "###,###,###,###.##";
        public static string CONFIG_AMOUNTOS_MASK_FORMAT_STRING = "{0:" + CONFIG_AMOUNTOS_MASK_FORMAT + "}";

        public static string CONFIG_AMOUNTOS_DISPLAY_FORMAT = "###,###,###,###.##";
        public static string CONFIG_AMOUNTOS_DISPLAY_FORMAT_STRING = "{0:" + CONFIG_AMOUNTOS_DISPLAY_FORMAT + "}";

        public static string CONFIG_AMOUNTOS_REPORT_FORMAT = "n2";
        public static string CONFIG_AMOUNTOS_REPORT_FORMAT_STRING = "{0:" + CONFIG_AMOUNTOS_REPORT_FORMAT + "}";

        #endregion

        #region DayUsing, DayRedo
        public static string CONFIG_DAYUSING_MASK_FORMAT = "#,###,###";
        public static string CONFIG_DAYUSING_MASK_FORMAT_STRING = "{0:" + CONFIG_DAYUSING_MASK_FORMAT + "}";

        public static string CONFIG_DAYUSING_DISPLAY_FORMAT = "#,###";
        public static string CONFIG_DAYUSING_DISPLAY_FORMAT_STRING = "{0:" + CONFIG_DAYUSING_DISPLAY_FORMAT + "}";

        public static string CONFIG_DAYUSING_REPORT_FORMAT = "n0";
        public static string CONFIG_DAYUSING_REPORT_FORMAT_STRING = "{0:" + CONFIG_PRICEVN_REPORT_FORMAT + "}";

        #endregion

        #region"Count"
        public static string CONFIG_COUNT_DISPLAY_FORMAT = "#,###,###,###.##";
        public static string CONFIG_COUNT_MASK_FORMAT_STRING = "{0:" + CONFIG_AMOUNTVN_MASK_FORMAT + "}";
        public static string CONFIG_COUNT_DISPLAY_FORMAT_STRING = "{0:" + CONFIG_QUANTITY_DISPLAY_FORMAT + "}";
        #endregion

        #region "decimal2"
        public static string CONFIG_DECIMAL2_DISPLAY_FORMAT = "#,###,###,##0.##";
        //public static string CONFIG_DECIMAL2_MASK_FORMAT_STRING = "{0:" + CONFIG_DECIMAL2_DISPLAY_FORMAT + "}";
        public static string CONFIG_DECIMAL2_DISPLAY_FORMAT_STRING = "{0:" + CONFIG_DECIMAL2_DISPLAY_FORMAT + "}";
        #endregion
    }

    public class FunctionName
    {
        public const string ItemGroups = "ItemGroup";
        public const string Items = "Item";
        public const string Customers = "CustomerEmployee";
        public const string ServiceGroups = "ServiceGroup";
        public const string Services = "Service";
        public const string DiscountCards = "DiscountCard";

        #region "Buttons WarrantyProcess Forms"
        ///Button WarrantyProcess Form 1->2
        public const string WarrantyProcess1go2 = "ButtonMakeList";
        public const string WarrantyProcess2back1 = "ButtonMakeCensor";
        public const string WarrantyProcess2go3 = "ButtonMakeCensor";
        public const string WarrantyWritingOldPhoto1 = "ButtonTheSignMent";
        public const string WarrantyWritingCurrentPhoto1 = "ButtonTheSignMent";
        public const string WarrantyWritingText2 = "ButtonTheSignMent";
        public const string WarrantyWritingOldPhoto2 = "ButtonTheSignMent";
        public const string WarrantyWritingCurrentPhoto2 = "ButtonTheSignMent";
        ///Button WarrantyProcess Form 3->4
        public const string WarrantyProcess3back2 = "ButtonRatifyMinor";
        public const string WarrantyProcess3go4 = "ButtonRatifyMinor";
        public const string WarrantyProcess4back3 = "ButtonRatifyMajor";
        public const string WarrantyProcess4go5 = "ButtonRatifyMajor";
        public const string WarrantyWritingOldPhoto3 = "ButtonTheSignMent";
        public const string WarrantyWritingCurrentPhoto3 = "ButtonTheSignMent";
        public const string WarrantyWritingText4 = "ButtonTheSignMent";
        public const string WarrantyWritingOldPhoto4 = "ButtonTheSignMent";
        public const string WarrantyWritingCurrentPhoto4 = "ButtonTheSignMent";
        ///Button WarrantyProcess Form 5->6
        public const string WarrantyProcess5back4 = "ButtonReBuild";
        public const string WarrantyProcess5go6 = "ButtonReBuild";
        public const string WarrantyProcess6back5 = "ButtonVerifyReBuild";
        public const string WarrantyProcess6go7 = "ButtonVerifyReBuild";
        public const string WarrantyWrirtingOldPhoto5 = "ButtonTheSignMent";
        public const string WarrantyWrirtingCurrentPhoto5 = "ButtonTheSignMent";
        public const string WarrantyWrirtingOldPhoto6 = "ButtonTheSignMent";
        public const string WarrantyWrirtingCurrentPhoto6 = "ButtonTheSignMent";
        ///Button WarrantyProcess Form 7
        public const string WarrantyProcess7back6 = "ButtonArchiveMent";
        public const string WarrantyWritingOldPhoto7 = "ButtonTheSignMent";
        public const string WarrantyWritingCurrentPhoto7 = "ButtonTheSignMent";
        #endregion "Buttons WarrantyProcess Forms"

        #region "Buttons RepairProcess Forms"
        ///Button RepairProcess Form 1->2
        public const string RepairProcess1go2 = "ButtonMakeList";
        public const string RepairProcess2back1 = "ButtonMakeCensor";
        public const string RepairProcess2go3 = "ButtonMakeCensor";
        public const string RepairWritingOldPhoto1 = "ButtonTheSignMent";
        public const string RepairWritingCurrentPhoto1 = "ButtonTheSignMent";
        public const string RepairWritingText2 = "ButtonTheSignMent";
        public const string RepairWritingOldPhoto2 = "ButtonTheSignMent";
        public const string RepairWritingCurrentPhoto2 = "ButtonTheSignMent";
        ///Button RepairProcess Form 3->4
        public const string RepairProcess3back2 = "ButtonRatifyMinor";
        public const string RepairProcess3go4 = "ButtonRatifyMinor";
        public const string RepairProcess4back3 = "ButtonRatifyMajor";
        public const string RepairProcess4go5 = "ButtonRatifyMajor";
        public const string RepairWritingText4 = "ButtonTheSignMent";
        public const string RepairWritingOldPhoto3 = "ButtonTheSignMent";
        public const string RepairWritingCurrentPhoto3 = "ButtonTheSignMent";
        public const string RepairWritingOldPhoto4 = "ButtonTheSignMent";
        public const string RepairWritingCurrentPhoto4 = "ButtonTheSignMent";
        ///Button RepairProcess Form 5
        public const string RepairProcess5back4 = "ButtonVerifyReBuild";
        public const string RepairProcess5go6 = "ButtonVerifyReBuild";
        ///Button RepairProcess Form 6
        public const string RepairProcess6back5 = "ButtonArchiveMent";
        #endregion "Buttons RepairProcess Forms"
    }
}
