using System;
using System.Collections.Generic;
using System.Windows.Forms;
using GS.UserManagement.GUI;

namespace QLCC
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            FormLogin f = new FormLogin();
            if(f.ShowDialog() == DialogResult.OK)
                Application.Run(new FormMain());
        }
    }
}