﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GS.UserManagement.GUI;
using QuanLySieuThi.Forms;

namespace QLCC
{
    public partial class FormMain : FormMainBase
    {
        public FormMain()
        {
            InitializeComponent();
        }

        /// <summary>
        /// I-Người sử dụng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuListUser_Click(object sender, EventArgs e)
        {
            FormListUser frm = new FormListUser();
            this.ShowForm(frm, ((ToolStripMenuItem)sender).Tag);
        }

        /// <summary>
        /// II-Cập nhật cơ sở dữ liệu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuUpdateDatabase_Click(object sender, EventArgs e)
        {
            //FormUpdateDB f = new FormUpdateDB();
            //this.ShowForm(f, ((ToolStripMenuItem)sender).Tag);
        }
        /// <summary>
        /// 22-Cập nhật danh mục
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuRefreshList_Click(object sender, EventArgs e)
        {
            GS.NV.Data.DanhMucBLL.RefreshDanhMucBLL();
            MessageBox.Show("Hoàn tất cập nhật 'Danh mục' !");
        }

        private void MenuCompanyInformation_Click(object sender, EventArgs e)
        {
            FormEditCompanyInformation frm = new FormEditCompanyInformation();
            this.ShowForm(frm, ((ToolStripMenuItem)sender).Tag);
        }

    }
}

