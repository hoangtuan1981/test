﻿namespace QLCC
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.MenuList = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuRefreshList = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.MenuManager = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuListUser = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuUpdateDatabase = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuCompanyInformation = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.defaultBarAndDocking.Controller)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(279, 24);
            this.listView1.Size = new System.Drawing.Size(545, 315);
            // 
            // treeView1
            // 
            this.treeView1.LineColor = System.Drawing.Color.Black;
            this.treeView1.Location = new System.Drawing.Point(0, 24);
            this.treeView1.Size = new System.Drawing.Size(279, 315);
            // 
            // defaultLookAndFeel
            // 
            this.defaultLookAndFeel.LookAndFeel.SkinName = "The Asphalt World";
            // 
            // defaultBarAndDocking
            // 
            this.defaultBarAndDocking.Controller.LookAndFeel.SkinName = "Stardust";
            this.defaultBarAndDocking.Controller.PaintStyleName = "Skin";
            // 
            // MenuList
            // 
            this.MenuList.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuCompanyInformation,
            this.toolStripSeparator3,
            this.MenuRefreshList});
            this.MenuList.Image = ((System.Drawing.Image)(resources.GetObject("MenuList.Image")));
            this.MenuList.Name = "MenuList";
            this.MenuList.Size = new System.Drawing.Size(84, 20);
            this.MenuList.Text = "Danh mục";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(167, 6);
            // 
            // MenuRefreshList
            // 
            this.MenuRefreshList.Image = ((System.Drawing.Image)(resources.GetObject("MenuRefreshList.Image")));
            this.MenuRefreshList.Name = "MenuRefreshList";
            this.MenuRefreshList.Size = new System.Drawing.Size(170, 22);
            this.MenuRefreshList.Text = "Cập nhật danh mục";
            this.MenuRefreshList.Click += new System.EventHandler(this.MenuRefreshList_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuList,
            this.MenuManager});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(824, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // MenuManager
            // 
            this.MenuManager.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuListUser,
            this.toolStripSeparator5,
            this.MenuUpdateDatabase});
            this.MenuManager.Image = ((System.Drawing.Image)(resources.GetObject("MenuManager.Image")));
            this.MenuManager.Name = "MenuManager";
            this.MenuManager.Size = new System.Drawing.Size(72, 20);
            this.MenuManager.Text = "Quản trị";
            // 
            // MenuListUser
            // 
            this.MenuListUser.Image = ((System.Drawing.Image)(resources.GetObject("MenuListUser.Image")));
            this.MenuListUser.Name = "MenuListUser";
            this.MenuListUser.Size = new System.Drawing.Size(183, 22);
            this.MenuListUser.Tag = "UserManagerment";
            this.MenuListUser.Text = "Người sử dụng";
            this.MenuListUser.Click += new System.EventHandler(this.MenuListUser_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(180, 6);
            // 
            // MenuUpdateDatabase
            // 
            this.MenuUpdateDatabase.Image = ((System.Drawing.Image)(resources.GetObject("MenuUpdateDatabase.Image")));
            this.MenuUpdateDatabase.Name = "MenuUpdateDatabase";
            this.MenuUpdateDatabase.Size = new System.Drawing.Size(183, 22);
            this.MenuUpdateDatabase.Text = "Cập nhật cơ sở dữ liệu";
            this.MenuUpdateDatabase.Click += new System.EventHandler(this.MenuUpdateDatabase_Click);
            // 
            // MenuCompanyInformation
            // 
            this.MenuCompanyInformation.Name = "MenuCompanyInformation";
            this.MenuCompanyInformation.Size = new System.Drawing.Size(170, 22);
            this.MenuCompanyInformation.Tag = "CompanyInformation";
            this.MenuCompanyInformation.Text = "Thông tin công ty";
            this.MenuCompanyInformation.Click += new System.EventHandler(this.MenuCompanyInformation_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.ClientSize = new System.Drawing.Size(824, 361);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormMain";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Controls.SetChildIndex(this.menuStrip1, 0);
            this.Controls.SetChildIndex(this.treeView1, 0);
            this.Controls.SetChildIndex(this.listView1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.defaultBarAndDocking.Controller)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem MenuList;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem MenuManager;
        private System.Windows.Forms.ToolStripMenuItem MenuListUser;
        private System.Windows.Forms.ToolStripMenuItem MenuUpdateDatabase;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem MenuRefreshList;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem MenuCompanyInformation;

    }
}
