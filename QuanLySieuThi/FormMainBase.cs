using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GS.Windows.Forms;
using GS.Data;

namespace QLCC
{
    public partial class FormMainBase : FormBase
    {
        public const string CONST_ERROR_NOT_ALLOW = "Rất tiếc, bạn không có đủ quyền để thực thi chức năng này";
        public FormMainBase()
        {
            InitializeComponent();
            
        }

        /// <summary>
        /// Load images from menu items to image list
        /// </summary>
        /// <param name="items"></param>
        private void LoadImages(ToolStripItemCollection items)
        {
            foreach (ToolStripItem item in items)
            {
                if (item.Image != null)
                {
                    this.imageList1.Images.Add(item.Name, item.Image);
                }
                if (item is ToolStripMenuItem)
                {
                    ToolStripMenuItem menuItem = item as ToolStripMenuItem;
                    if (menuItem.HasDropDownItems)
                    {
                        LoadImages(menuItem.DropDownItems);
                    }
                }
            }
        }

        /// <summary>
        /// Adds root nodes and children nodes from menu to treeview
        /// </summary>
        /// <param name="menu"></param>
        /// <param name="rootNode"></param>
        private void LoadTreeView(MenuStrip menu, string rootNode)
        {
            TreeNode node = new TreeNode(rootNode);
            node.ImageIndex = node.SelectedImageIndex = 0;
            this.treeView1.Nodes.Add(node);
            foreach (ToolStripItem item in menu.Items)
            {
                if (item is ToolStripMenuItem)
                {
                    if ((item as ToolStripMenuItem).HasDropDownItems)
                        LoadTreeView(item as ToolStripMenuItem, node);
                }
            }
        }

        /// <summary>
        /// Adds menu item which has sub items to treeview
        /// </summary>
        /// <param name="menuItem"></param>
        /// <param name="parent"></param>
        private void LoadTreeView(ToolStripMenuItem menuItem, TreeNode parent)
        {
            if (!menuItem.Enabled)
                return;
            TreeNode node = new TreeNode(menuItem.Text);
            node.ImageKey = node.SelectedImageKey = menuItem.Name;
            node.ToolTipText = menuItem.ToolTipText;
            if (parent == null)
                this.treeView1.Nodes.Add(node);
            else
                parent.Nodes.Add(node);
            node.Tag = menuItem;
            foreach (ToolStripItem item in menuItem.DropDownItems)
            {
                if (item is ToolStripMenuItem)
                {
                    if ((item as ToolStripMenuItem).HasDropDownItems)
                        LoadTreeView(item as ToolStripMenuItem, node);
                }
            }
        }

        /// <summary>
        /// Adds menu items and sub-items to listview under a group
        /// </summary>
        /// <param name="item"></param>
        /// <param name="group"></param>
        private void AddMenuItemToList(ToolStripItem item, ListViewGroup group)
        {
            if (!item.Enabled)
                return;
            if (item is ToolStripMenuItem)
            {
                ListViewItem lvItem = new ListViewItem(item.Text);
                lvItem.Group = group;
                listView1.Items.Add(lvItem);
                lvItem.Tag = item;
                lvItem.ImageKey = item.Name;
                lvItem.ToolTipText = item.ToolTipText;

                if ((item as ToolStripMenuItem).HasDropDownItems)
                    LoadListView(item as ToolStripMenuItem, false);
            }
        }

        /// <summary>
        /// Adds all menu items and sub-items to listview
        /// </summary>
        /// <param name="menu"></param>
        /// <param name="defaultGroup"></param>
        private void LoadListView(MenuStrip menu, string defaultGroup)
        {
            this.listView1.Items.Clear();
            this.listView1.Groups.Clear();

            ListViewGroup group = new ListViewGroup(defaultGroup);
            this.listView1.Groups.Add(group);
            foreach (ToolStripItem item in menu.Items)
            {
                AddMenuItemToList(item, group);
            }
        }

        /// <summary>
        /// Adds a menu items and its sub-items to list view
        /// </summary>
        /// <param name="menuItem"></param>
        /// <param name="clear"></param>
        private void LoadListView(ToolStripMenuItem menuItem, bool clear)
        {
            if (clear)
            {
                this.listView1.Items.Clear();
                this.listView1.Groups.Clear();
            }

            ListViewGroup group = new ListViewGroup(menuItem.Text);
            this.listView1.Groups.Add(group);
            foreach (ToolStripItem item in menuItem.DropDownItems)
            {
                AddMenuItemToList(item, group);
            }
        }

        private void GenTabButtons()
        {
            this.imageList1.ColorDepth = ColorDepth.Depth32Bit;
            this.imageList1.ImageSize = new Size(32, 32);
            LoadImages(this.MainMenuStrip.Items);


            this.listView1.LargeImageList = this.imageList1;
            this.treeView1.ImageList = this.imageList1;
            LoadTreeView(this.MainMenuStrip, this.Text);
            LoadListView(this.MainMenuStrip, this.Text);

            this.treeView1.HideSelection = false;
            //this.treeView1.AfterCollapse += new TreeViewEventHandler(treeView1_AfterCollapse);
            this.treeView1.AfterSelect += new TreeViewEventHandler(treeView1_AfterSelect);
            this.treeView1.ExpandAll();

            //this.listView1.KeyUp += new KeyEventHandler(listView1_KeyUp);
            this.listView1.MouseClick += new MouseEventHandler(listView1_MouseClick);
            this.listView1.LabelEdit = false;
            this.listView1.Cursor = Cursors.Default;
            this.listView1.HotTracking = true;

        }
        void listView1_MouseClick(object sender, MouseEventArgs e)
        {
            ListViewItem item = this.listView1.GetItemAt(e.X, e.Y);
            ShowItem(item);

        }

        /// <summary>
        /// Invokes the selected listview item action
        /// </summary>
        /// <param name="item"></param>
        private void ShowItem(ListViewItem item)
        {
            ToolStripMenuItem menuItem = item.Tag as ToolStripMenuItem;
            if (menuItem.HasDropDownItems)
                LoadListView(menuItem, true);
            else
            {
                if ((item.Tag as ToolStripMenuItem).Enabled)
                    (item.Tag as ToolStripMenuItem).PerformClick();
                else
                    MessageBox.Show(CONST_ERROR_NOT_ALLOW, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Tag == null)
                LoadListView(this.MainMenuStrip, this.Text);
            else
                LoadListView(e.Node.Tag as ToolStripMenuItem, true);
        }

        void listView1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && e.Modifiers == Keys.None && this.listView1.SelectedItems.Count > 0)
                ShowItem(this.listView1.SelectedItems[0]);
        }


        void listView1_ItemMouseHover(object sender, ListViewItemMouseHoverEventArgs e)
        {
            this.listView1.Cursor = Cursors.Default;
            if (e.Item.Tag is ToolStripMenuItem)
                if ((e.Item.Tag as ToolStripMenuItem).Enabled)
                {
                    this.listView1.Cursor = Cursors.Hand;

                }

        }

        void treeView1_AfterCollapse(object sender, TreeViewEventArgs e)
        {
            this.treeView1.ExpandAll();
        }

        protected bool SetMemberFunction(ToolStripMenuItem oControl)
        {
            bool bReturn = true;
            if (oControl.Tag != null && oControl.Tag.ToString() != string.Empty)
            {
                MemberFunction mf = ContextBase.CurrentUser.ListPrivilege.Search("FunctionID", oControl.Tag.ToString());
                if (mf != null)
                {
                    //oControl.Enabled = mf.AllowView;
                    bReturn = mf.AllowView;
                }
                else
                {
                    //oControl.Enabled = false;
                    bReturn = false;
                }
            }
            //else
            //{
            //    bReturn = oControl.Enabled;
            //}
            if (bReturn)
            {
                if (oControl.DropDownItems.Count > 0)
                {
                    bReturn = false;
                    foreach (ToolStripItem tsi in oControl.DropDownItems)
                    {
                        if (tsi is ToolStripMenuItem)
                        {
                            bool bTemp = SetMemberFunction((ToolStripMenuItem)tsi);
                            if (bTemp)
                                bReturn = true;
                        }
                    }
                }
            }
            oControl.Enabled = bReturn;
            return bReturn;
        }
        protected void SetMemberFunction(Control oControl)
        {
            foreach (Control ctrl in oControl.Controls)
            {
                if (ctrl is MenuStrip)
                {
                    foreach (ToolStripMenuItem tsmi in (ctrl as MenuStrip).Items)
                    {

                        SetMemberFunction(tsmi);
                    }
                }

            }
        }
        protected void SetFormPrivilege(Form f,object FunctionName)
        {
            if (ContextBase.CurrentUser.IsAdmin == false)
            {
                if (!ContextBase.CurrentUser.IsAdmin && FunctionName != null)
                {
                    if (f is FormEditBase)
                    {
                        FormEditBase fe = f as FormEditBase;
                        MemberFunction mf = ContextBase.CurrentUser.ListPrivilege.Search("FunctionID", FunctionName.ToString());
                        if (mf != null)
                        {
                            fe.AllowAddNew = mf.AllowAdd && fe.AllowAddNew;
                            fe.AllowEdit = mf.AllowEdit && fe.AllowEdit;
                            fe.AllowDelete = mf.AllowDelete && fe.AllowDelete;
                        }
                    }
                }
            }
        }
        private void FormMainBase_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                if (!ContextBase.CurrentUser.IsAdmin)
                    this.SetMemberFunction(this);
                GenTabButtons();
            }

        }

        protected void ShowForm(Form f,object tag)
        {
            SetFormPrivilege(f, tag);
            //this.AddOwnedForm(f);
            //f.MdiParent = this;
            //f.ShowInTaskbar = false;
            f.Owner = this;
            f.Show();
            //f.ShowDialog(this);
            //this.ShowInTaskbar = true;
        }
    }
}