using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Columns;

using GS.Data;



namespace GS.Windows.Forms
{
    public partial class FormBase : DevExpress.XtraEditors.XtraForm
    {
        private XmlDocument messageDoc = new XmlDocument();

        public XmlDocument MessageDocument
        {
            get { return messageDoc; }
            set { messageDoc = value; }
        }

        private string m_MessagePrefix;
        [Browsable(true)]
        [Category("GS - Properties")]
        public string MessagePrefix
        {
            get { return m_MessagePrefix; }
            set { m_MessagePrefix = value; }
        }
        private string m_LayoutFile;

        [Browsable(true)]
        [Category("GS - Properties")]
        public string LayoutFile
        {
            get { return m_LayoutFile; }
            set { m_LayoutFile = value; }
        }

        public string LayoutSuffix = string.Empty;

        public FormBase()
        {

            InitializeComponent();

            LayoutFile = this.GetType().Name + ".xml";
            MessagePrefix = this.GetType().Name + "-";
        }

        public string GetTextMessage(string id, string defaultValue)
        {
            if (messageDoc != null)
                return XMLFunctions.GetTextFromXML(messageDoc, id, LayoutDefines.XML_ATT_TEXT, defaultValue);
            //return XMLFunctions.GetTextFromXML(messageDoc, MessagePrefix + id, LayoutDefines.XML_ATT_TEXT, defaultValue);
            else
                return defaultValue;
        }
        public string GetTextMessage(string id, string defaultValue, string xmlFile)
        {
            XmlDocument xmlDoc = new XmlDocument();
            if (System.IO.File.Exists(Application.StartupPath + "\\xml\\" + ContextBase.LanguageFolder + "\\" + xmlFile))
            {
                xmlDoc.Load(Application.StartupPath + "\\xml\\" + ContextBase.LanguageFolder + "\\" + xmlFile);

                defaultValue = XMLFunctions.GetTextFromXML(xmlDoc, id, LayoutDefines.XML_ATT_TEXT, defaultValue);
            }
            return defaultValue;
        }

        private void frmBase_Load(object sender, EventArgs e)
        {
            //#if (DEBUG)
            //            LayoutDefines.GenXMLFormLayout(this, Generals.XmlFolder + "\\" + LayoutFile);
            //#endif
            //            if (System.IO.File.Exists(Generals.XmlFolder + "\\" + LayoutFile))
            //                LayoutDefines.SetFormLayout(this,LayoutFile);
            if (!this.DesignMode)
            {
                DisplayLanguage();
            }
        }
        protected void DisplayLanguage()
        {
            if (System.IO.File.Exists(Application.StartupPath + "\\xml\\" + ContextBase.LanguageFolder + "\\" + LayoutFile))
                LayoutDefines.SetFormLayout(this, Application.StartupPath + "\\xml\\" + ContextBase.LanguageFolder + "\\" + LayoutFile);
            if (System.IO.File.Exists(Application.StartupPath + "\\xml\\" + ContextBase.LanguageFolder + "\\" + LayoutFile))
                messageDoc.Load(Application.StartupPath + "\\xml\\" + ContextBase.LanguageFolder + "\\" + LayoutFile);
        }
        public void SetVisible(bool b)
        {
            this.Visible = b;
            if (this.Owner != null)
            {
                if (this.Owner is FormBase)
                {
                    (this.Owner as FormBase).SetVisible(b);
                }
            }
        }
        public void ShowChildForm(Form f)
        {
            //this.AddOwnedForm(f);
            //f.ShowDialog(this);
            f.Show(this.Owner);
        }
        public virtual void RefreshData()
        { }

        private void FormBase_Activated(object sender, EventArgs e)
        {
            RefreshData();
        }

        private void FormBase_KeyDown(object sender, KeyEventArgs e)
        {
            Control c = this.ActiveControl;
            #region set
            if (c.Parent is DevExpress.XtraEditors.DateEdit)
                return;
            if (c is DevExpress.XtraGrid.GridControl)
                return;
            if (c.Parent is DevExpress.XtraEditors.LookUpEdit)
            {
                if (e.KeyCode == Keys.Left)
                {
                    SendKeys.Send("+{TAB}");
                    e.Handled = true;
                }
                if (e.KeyCode == Keys.Right)
                {
                    SendKeys.Send("{TAB}");
                    e.Handled = true;
                }
                return;
            }
            if (c.Parent.Parent is DevExpress.XtraGrid.GridControl)
                return;
            #endregion
            if ((c as UserControl) != null)
            {
                c = (c as UserControl).ActiveControl;
                #region set
                if (c.Parent is DevExpress.XtraEditors.DateEdit)
                    return;
                if (c is DevExpress.XtraGrid.GridControl)
                    return;
                if (c.Parent is DevExpress.XtraEditors.LookUpEdit)
                {
                    if (e.KeyCode == Keys.Left)
                    {
                        SendKeys.Send("+{TAB}");
                        e.Handled = true;
                    }
                    if (e.KeyCode == Keys.Right)
                    {
                        SendKeys.Send("{TAB}");
                        e.Handled = true;
                    }
                    return;
                }
                if (c.Parent.Parent is DevExpress.XtraGrid.GridControl)
                    return;
                #endregion
                if ((c as UserControl) != null)
                {
                    c = (c as UserControl).ActiveControl;
                    #region set
                    if (c.Parent is DevExpress.XtraEditors.DateEdit)
                        return;
                    if (c is DevExpress.XtraGrid.GridControl)
                        return;
                    if (c.Parent is DevExpress.XtraEditors.LookUpEdit)
                    {
                        if (e.KeyCode == Keys.Left)
                        {
                            SendKeys.Send("+{TAB}");
                            e.Handled = true;
                        }
                        if (e.KeyCode == Keys.Right)
                        {
                            SendKeys.Send("{TAB}");
                            e.Handled = true;
                        }
                        return;
                    }
                    if (c.Parent.Parent is DevExpress.XtraGrid.GridControl)
                        return;
                    #endregion
                }
            }
            switch (e.KeyCode)
            {
                case Keys.Up:
                    SendKeys.Send("+{TAB}");
                    e.Handled = true;
                    break;
                case Keys.Down:
                    SendKeys.Send("{TAB}");
                    e.Handled = true;
                    break;
            }
        }
        static string TextUserCreated;
        static string TextUserUpdated;
        static string TextDateCreated;
        static string TextDateUpdated;
        static string TipUserCreated;
        static string TipUserUpdated;
        static string TipDateCreated;
        static string TipDateUpdated;
        private void GetTextXML()
        {
            XmlDocument xmlDocTemp = new XmlDocument();
            xmlDocTemp.Load(Application.StartupPath + "\\xml\\" + ContextBase.LanguageFolder + "\\DefaultFormEditBase.xml");

            TextUserCreated = XMLFunctions.GetTextFromXML(xmlDocTemp, "colUserCreated", LayoutDefines.XML_ATT_TEXT, "UserCreated");
            TextUserUpdated = XMLFunctions.GetTextFromXML(xmlDocTemp, "colUserUpdated", LayoutDefines.XML_ATT_TEXT, "UserUpdated");
            TextDateCreated = XMLFunctions.GetTextFromXML(xmlDocTemp, "colDateCreated", LayoutDefines.XML_ATT_TEXT, "DateCreated");
            TextDateUpdated = XMLFunctions.GetTextFromXML(xmlDocTemp, "colDateUpdated", LayoutDefines.XML_ATT_TEXT, "DateUpdated");

            TipUserCreated = XMLFunctions.GetTextFromXML(xmlDocTemp, "colUserCreated", LayoutDefines.XML_ATT_TOOLTIPTEXT, "UserCreated");
            TipUserUpdated = XMLFunctions.GetTextFromXML(xmlDocTemp, "colUserUpdated", LayoutDefines.XML_ATT_TOOLTIPTEXT, "UserUpdated");
            TipDateCreated = XMLFunctions.GetTextFromXML(xmlDocTemp, "colDateCreated", LayoutDefines.XML_ATT_TOOLTIPTEXT, "DateCreated");
            TipDateUpdated = XMLFunctions.GetTextFromXML(xmlDocTemp, "colDateUpdated", LayoutDefines.XML_ATT_TOOLTIPTEXT, "DateUpdated");
        }
        protected void AddUserTrackingColumn(GridView gv)
        {
            if (TextUserCreated == null)
                GetTextXML();
            GridColumn colUserCreated = gv.Columns.Add();
            colUserCreated.Caption = TextUserCreated;
            colUserCreated.ToolTip = TipUserCreated;
            colUserCreated.Visible = false;
            colUserCreated.FieldName = "UserCreated";
            colUserCreated.OptionsColumn.AllowEdit = false;

            GridColumn colDateCreated = gv.Columns.Add();
            colDateCreated.Caption = TextDateCreated;
            colDateCreated.ToolTip = TipDateCreated;
            colDateCreated.Visible = false;
            colDateCreated.FieldName = "DateCreated";
            colDateCreated.OptionsColumn.AllowEdit = false;
            colDateCreated.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            colDateCreated.DisplayFormat.FormatString = AppConfigBase.CONFIG_DATEFORMAT + " H:mm:ss";

            GridColumn colUserUpdated = gv.Columns.Add();
            colUserUpdated.Caption = TextUserUpdated;
            colUserUpdated.ToolTip = TipUserUpdated;
            colUserUpdated.Visible = false;
            colUserUpdated.FieldName = "UserUpdated";
            colUserUpdated.OptionsColumn.AllowEdit = false;

            GridColumn colDateUpdated = gv.Columns.Add();
            colDateUpdated.Caption = TextDateUpdated;
            colDateUpdated.ToolTip = TipDateUpdated;
            colDateUpdated.Visible = false;
            colDateUpdated.FieldName = "DateUpdated";
            colDateUpdated.OptionsColumn.AllowEdit = false;
            colDateUpdated.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            colDateUpdated.DisplayFormat.FormatString = AppConfigBase.CONFIG_DATEFORMAT + " H:mm:ss";
        }

    }
}