using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;
//using VNS.Utils;

namespace GS.Data
{
    class MemberFunctionDAL:BaseDAL<MemberFunction>
    {
        public MemberFunctionDAL()
        { }
        public MemberFunctionDAL(DBHelper dbHelper)
            : base(dbHelper)
        { }

        /// <summary>
        /// Inserts an object into database by calling Insert StoredProcedure
        /// </summary>
        public override int Insert(MemberFunction t)
        {
            int iError = 0;
            bool alreadyOpen = false;
            try
            {
                if (db.State != System.Data.ConnectionState.Open)
                    db.Open();
                else
                    alreadyOpen = true;
                DbCommand cmd = db.CreateCommand();
                cmd.CommandText = "usp_MemberFunction_Insert";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                cmd.Parameters.Add(db.CreateParameter("@MemberID", System.Data.DbType.AnsiString, 20, t.MemberID));
                cmd.Parameters.Add(db.CreateParameter("@FunctionID", System.Data.DbType.AnsiString, 20, t.FunctionID));
                cmd.Parameters.Add(db.CreateParameter("@AllowView", System.Data.DbType.Boolean, 1, t.AllowView));
                cmd.Parameters.Add(db.CreateParameter("@AllowAdd", System.Data.DbType.Boolean, 1, t.AllowAdd));
                cmd.Parameters.Add(db.CreateParameter("@AllowEdit", System.Data.DbType.Boolean, 1, t.AllowEdit));
                cmd.Parameters.Add(db.CreateParameter("@AllowDelete", System.Data.DbType.Boolean, 1, t.AllowDelete));

                cmd.Parameters.Add(db.CreateParameter("@iError", System.Data.DbType.Int32, 4, 0, System.Data.ParameterDirection.Output));

                iError = db.ExecuteNonQuery(cmd);
                //if (iError == 0)
                iError = (int)cmd.Parameters["@iError"].Value;
                if (iError == 0)
                {
                }
            }
            catch (Exception excp)
            {
                iError = -1000;
                Write2Log.WriteLogs("MemberFunctionDAL", "Insert(MemberFunction t)", excp.Message);
            }
            finally
            {
                if (!alreadyOpen)
                    db.Close();
            }
            return iError;
        }

        public int DeleteByMemberID(string memberID)
        {
            int iError = 0;
            bool alreadyOpen = false;
            try
            {
                if (db.State != System.Data.ConnectionState.Open)
                    db.Open();
                else
                    alreadyOpen = true;
                DbCommand cmd = db.CreateCommand();
                cmd.CommandText = "usp_MemberFunction_DeleteByMemberID";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(db.CreateParameter("@MemberID", System.Data.DbType.AnsiString, 20, memberID));
                cmd.Parameters.Add(db.CreateParameter("@iError", System.Data.DbType.Int32, 4, 0, System.Data.ParameterDirection.Output));

                iError = db.ExecuteNonQuery(cmd);
                //if (iError == 0)
                iError = (int)cmd.Parameters["@iError"].Value;
            }
            catch (Exception excp)
            {
                iError = -1000;
                Write2Log.WriteLogs("MemberFunctionDAL", "Delete(MemberFunction t)", excp.Message);
            }
            finally
            {
                if (!alreadyOpen)
                    db.Close();
            }
            return iError;
        }

        public ListBase<MemberFunctionEdit> GetMemberFunctionEdit(string memberID)
        {
            int iError = 0;
            bool alreadyOpen = false;
            ListBase<MemberFunctionEdit> lst = new ListBase<MemberFunctionEdit>();
            try
            {
                DbDataReader reader = null;
                if (db.State != System.Data.ConnectionState.Open)
                    db.Open();
                else
                    alreadyOpen = true;
                DbCommand cmd = db.CreateCommand();
                cmd.CommandText = "usp_MemberFunctionEdit_SelectByMemberID";
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(db.CreateParameter("@MemberID", System.Data.DbType.AnsiString, 20, memberID));

                reader = db.ExecuteReader(cmd);
                while (reader.Read())
                {
                    lst.Add(new MemberFunctionEdit(reader));
                }
            }
            catch (Exception excp)
            {
                Write2Log.WriteLogs("MemberFunctionDAL", "GetMemberFunctionEdit(string memberID)", excp.Message);
            }
            finally
            {
                if (!alreadyOpen)
                    db.Close();
            }
            return lst;
        }
    }
}
