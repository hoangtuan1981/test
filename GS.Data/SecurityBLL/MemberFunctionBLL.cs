using System;
using System.Collections.Generic;
using System.Text;

namespace GS.Data
{
    public class MemberFunctionBLL : IBusiness
    {
        MemberFunctionDAL dal = new MemberFunctionDAL();

        public ListBase<MemberFunctionEdit> GetListMemberFunctionEdit(string memberID)
        {
            return dal.GetMemberFunctionEdit(memberID);
        }
        public int Update(Member t)
        {
            int iError = 0;
            
            try
            {
                dal.Open();
                dal.BeginTransaction();
                iError = dal.DeleteByMemberID(t.MemberID);
                if (iError == 0)
                {
                    foreach (MemberFunctionEdit d in t.ListMemberFunctionEdit)
                    {
                        if (d.AllowView)
                        {
                            d.MemberID = t.MemberID;
                            iError = dal.Insert(d);
                            if (iError != 0)
                                break;
                        }
                    }
                }
            }
            catch
            {
                dal.Rollback();
            }
            finally
            {
                if (iError == 0)
                    dal.Commit();
                else
                    dal.Rollback();
                dal.Close();
            }
            return iError;
        }

        #region IBusiness Members

        public int Insert(object obj)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public int Update(object obj)
        {
            return this.Update(obj as Member);
        }

        public int Delete(object obj)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}
