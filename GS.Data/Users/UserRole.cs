using System;
using System.Collections.Generic;
using System.Text;

namespace GS.Data
{
    public class UserRole : ObjectBase
    {
        protected string _RoleName = String.Empty;
        public string RoleName
        {
            get { return _RoleName; }
            set { _RoleName = value; }
        }
    }
}