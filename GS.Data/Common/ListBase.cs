using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Collections;
using System.Reflection;
using System.Reflection.Emit;
namespace GS.Data
{
    public interface IListBase
    {

        int CurrentIndex { get; set; }

        object CurrentItem { get; set;}

        void OnPositionChanged(int oldIndex, int newIndex);
    }
    public class ListBase<T>:BindingList<T>,IListBase 
    {
        public event IndexChangedEventHander PositionChanged;
        
        public void OnPositionChanged(int oldIndex, int newIndex)
        {
            if (this.PositionChanged != null)
                this.PositionChanged(oldIndex, newIndex);
        }

        public object CurrentItem
        {
            get {
                if (this.currentIndex < this.Count && this.currentIndex>-1)
                    return this[currentIndex];
                else return default(T);
            }
            set {
                if (this.Contains((T)value ))
                {
                    int oldIndex = this.currentIndex;
                    this.currentIndex = this.IndexOf((T)value );
                    OnPositionChanged(oldIndex, this.currentIndex);
                }
                else
                    throw new IndexOutOfRangeException(); 
            }
        }

        private int currentIndex;

        public int CurrentIndex
        {
            get { return currentIndex; }
            set
            {
                int oldIndex = this.currentIndex;
                currentIndex = value;
                OnPositionChanged(oldIndex, this.currentIndex); 
                
            }
        }
	
        public ListBase()
        {
            
        }

        protected override void OnListChanged(ListChangedEventArgs e)
        {
            base.OnListChanged(e);
            if (this.Count > 0)
                this.currentIndex = 0;
            else
                this.currentIndex = -1;
        }
        /// <summary>
        /// //////////////
        /// </summary>
        List<ObjectSortProperty> objectSortProperties;
        public void Sort(List<ObjectSortProperty> sortProps)
        {
            if (this.SupportsSortingCore)
            {
                objectSortProperties = new List<ObjectSortProperty>();
                //SetNewCollection(itemCollection, objectSortProperties);
                objectSortProperties = sortProps;
                //AddObjectSortPropertiesToList(objectSortProperties);
                 List<T> items = this.Items as List<T>;
                items.Sort(CompareValuesMultiProperty);
            }
        }

        private int CompareValuesMultiProperty(T o1, T o2)
        {
            int sortResult = 0;
            List<ObjectSortProperty>.Enumerator enmr = objectSortProperties.GetEnumerator();
            while (sortResult == 0 && enmr.MoveNext())
            {
                PropertyValueDelegate<T> GetPropValue = GetPropertyValueDelegate<T>(enmr.Current.PropertyName);
                object obj1 = GetPropValue(o1);
                object obj2 = GetPropValue(o2);
                // reflection Method - Old
                /*object obj1 = typeof(TObject).GetProperty(enmr.Current.PropertyName).GetValue(o1, null);
                object obj2 = typeof(TObject).GetProperty(enmr.Current.PropertyName).GetValue(o2, null);*/
                if (enmr.Current.SortOn == SortOnEnum.String)
                {
                    obj1 = obj1.ToString();
                    obj2 = obj2.ToString();
                }
                // if sort order is descending then reverse the result.
                sortResult = enmr.Current.SortOrderMultiplier * ((IComparable)obj1).CompareTo(obj2);
            }
            enmr.Dispose();
            return sortResult;
        }
        #region IL generator
        public delegate object PropertyValueDelegate<TObj>(TObj obj);
        public static PropertyValueDelegate<TObj> CreatePropertyValueDelegate<TObj>(string propertyName)
        {
            // Use Reflection to get the MethodInfo for our Property's get accessor
            MethodInfo mi = typeof(TObj).GetMethod("get_" + propertyName);
            if (mi == null)
                throw new Exception("Property '" + propertyName + "' doesn't have a get implementation");

            // Create a new Dynamic Method for retrieving the Property's value
            DynamicMethod dm = new DynamicMethod("Get" + propertyName + "PropertyValue",
                typeof(object), new Type[] { typeof(TObj) }, typeof(TObj).Module);
            // Let's Emit some IL code to actually call the Property's accessor and return the value.
            ILGenerator il = dm.GetILGenerator();
            // Load arg 0 (Object) onto the stack
            il.Emit(OpCodes.Ldarg_0);
            // Call the property's get accessor (method) on the object that was loaded onto the stack
            il.EmitCall(OpCodes.Call, mi, null);
            // if the property's get accessor (method) returns a value(int, datetime, etc) type then we have to box it
            if (mi.ReturnType.IsValueType)
                il.Emit(OpCodes.Box, mi.ReturnType);

            // Return the value (back to the caller) that was returned (to us) from calling the method above
            il.Emit(OpCodes.Ret);
            // Turn the Dynamic Method into a delegate and return it
            return (PropertyValueDelegate<TObj>)dm.CreateDelegate(typeof(PropertyValueDelegate<TObj>));
        }
        private static Hashtable StoredDelegatesHashTable = new Hashtable();
        public static PropertyValueDelegate<TObj> GetPropertyValueDelegate<TObj>(string propertyName)
        {
            object returnValue = StoredDelegatesHashTable[propertyName];
            if (returnValue == null)
            {
                returnValue = CreatePropertyValueDelegate<TObj>(propertyName);
                StoredDelegatesHashTable[propertyName] = returnValue;
            }
            return (PropertyValueDelegate<TObj>)returnValue;
        }
        #endregion
        //private void AddObjectSortPropertiesToList(IEnumerable<ObjectSortProperty> objectSortProperties)
        //{
        //    objectSortOnProperties.Clear();
        //    foreach (ObjectSortProperty property in objectSortProperties)
        //        objectSortOnProperties.Add(property);
        //}

        private static List<ObjectSortProperty> ParseOrderBy(string orderBy)
        {
            if (orderBy == null || orderBy.Length == 0)
                return new List<ObjectSortProperty>(0);
            string[] props = orderBy.Split(new char[] { ',' } , StringSplitOptions.RemoveEmptyEntries);
            List<ObjectSortProperty> properties = new List<ObjectSortProperty>(props.Length);
            for (int i = 0; i < props.Length; i++)
            {
                string prop = props[i].Trim();
                string[] words = prop.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (words.Length > 2)
                    throw new ArgumentException("Too many words in sort expression", "orderBy");
                if (words.Length == 2 && (!words[1].Equals("DESC", StringComparison.OrdinalIgnoreCase) && !words[1].Equals("ASC", StringComparison.OrdinalIgnoreCase)))
                    throw new ArgumentException("The second word in the sort expression must be ASC or DESC", "orderBy");
                if (words.Length == 1 || words[1].Equals("ASC", StringComparison.OrdinalIgnoreCase))
                    properties.Add(new ObjectSortProperty(words[0], SortOrderEnum.ASC));
                else
                    properties.Add(new ObjectSortProperty(words[0], SortOrderEnum.DESC));
            }
            return properties;
        }
        /// ///////////////
        
        public void Sort(string propertyName,ListSortDirection direction)
        {
            if (this.SupportsSortingCore)
            {
                PropertyDescriptor pro = TypeDescriptor.GetProperties(typeof(T)).Find(propertyName,true);
                if (pro != null)
                    this.ApplySortCore(pro, direction);
            }
        }

        public T Search(string propertyName, object value)
        {
            if (this.SupportsSearchingCore && this.Count>0)
            {
                
                PropertyDescriptor pro = TypeDescriptor.GetProperties(typeof(T)).Find(propertyName, true);
                if (pro != null)
                {
                    int index = this.FindCore(pro, value);
                    if (index >= 0)
                        return this[index];
                    else
                        return default(T);

                }
                else
                    return default(T);
            }
            else
                return default(T);
            
        }
        protected override int FindCore(PropertyDescriptor prop, object key)
        {
            
            foreach (T obj in this.Items)
            {
                
                if (prop.GetValue(obj).Equals(key))
                {
                    return this.IndexOf(obj);
                }
            }
            return -1;
        }
        protected override bool SupportsSearchingCore
        {
            get
            {
                return true;
            }
        }


        protected override bool SupportsSortingCore
        {
            get
            {
                return true;
            }
        }
        protected override void ApplySortCore(PropertyDescriptor prop, ListSortDirection direction)
        {
            PropertyComparer<T> comparer = new PropertyComparer<T>(prop.Name,direction);
            List<T> items = this.Items as List<T>;
            items.Sort(comparer);
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemIndex"></param>
        public override void EndNew(int itemIndex)
        {
            base.EndNew(itemIndex);
            
        }

        #region ICloneable Members

        public object Clone()
        {
            ListBase<T> lstObj = new ListBase<T>();
            foreach (T t in this.Items)
            {
                lstObj.Add((T)((ICloneable)t).Clone());
            }
            return lstObj;
        }

        #endregion
        
    }
    public delegate void IndexChangedEventHander(int oldIndex, int newIndex); 
}
