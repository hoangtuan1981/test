using System;
using System.Collections.Generic;
using System.Text;

namespace GS.Data
{
    public interface IBusiness
    {
        int Insert(object obj);
        int Update(object obj);
        int Delete(object obj);
    }
}
