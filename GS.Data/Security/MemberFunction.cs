using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace GS.Data
{
    /// <summary>
    /// the class to manage the privilege of a MemberID to access a FunctionID
    /// </summary>
    public class MemberFunction:ObjectBase
    {
        public MemberFunction()
        {
        }

        public MemberFunction(IDataReader reader)
        {
            this.FromDataReader(reader);
        }

        public override void CopyFrom(object obj)
        {
            base.CopyFrom(obj);

            memberID = (obj as MemberFunction).memberID;
            functionID = (obj as MemberFunction).functionID;
            allowView = (obj as MemberFunction).allowView;
            allowAdd = (obj as MemberFunction).allowAdd;
            allowEdit = (obj as MemberFunction).allowEdit;
            allowDelete = (obj as MemberFunction).allowDelete;

        }

        public override void FromDataReader(System.Data.IDataReader reader)
        {
            base.FromDataReader(reader);
            if (reader != null && !reader.IsClosed)
            {
                if (!isNull("FunctionID", reader)) functionID = reader.GetString(reader.GetOrdinal("FunctionID"));
                if (!isNull("MemberID", reader)) memberID = reader.GetString(reader.GetOrdinal("MemberID"));
                if (!isNull("AllowView", reader)) allowView = reader.GetBoolean(reader.GetOrdinal("AllowView"));
                if (!isNull("AllowAdd", reader)) allowAdd = reader.GetBoolean(reader.GetOrdinal("AllowAdd"));
                if (!isNull("AllowEdit", reader)) allowEdit = reader.GetBoolean(reader.GetOrdinal("AllowEdit"));
                if (!isNull("AllowDelete", reader)) allowDelete = reader.GetBoolean(reader.GetOrdinal("AllowDelete"));
            }
        }
        public override void FromDataRow(DataRow row)
        {
            base.FromDataRow(row);

            if (!row.IsNull("MemberID")) memberID = (string)row["MemberID"];
            if (!row.IsNull("FunctionID")) functionID = (string)row["FunctionID"];
            if (row.Table.Columns.Contains("AllowView"))
            {
                if (!row.IsNull("AllowView")) allowView = (bool)row["AllowView"];
                if (!row.IsNull("AllowAdd")) allowAdd = (bool)row["AllowAdd"];
                if (!row.IsNull("AllowEdit")) allowEdit = (bool)row["AllowEdit"];
                if (!row.IsNull("AllowDelete")) allowDelete = (bool)row["AllowDelete"];
            }
            if (row.Table.Columns.Contains("Description"))
                if (!row.IsNull("Description"))
                {
                    memberFunctionEdit = new MemberFunctionEdit();
                    memberFunctionEdit.FromDataRow(row);
                }
        }
        private string functionID = string.Empty;
        public string FunctionID
        {
            get { return functionID; }
            set { functionID = value; }
        }

        private string memberID = string.Empty;
        public string MemberID
        {
            get { return memberID; }
            set { memberID = value; }
        }

        private bool allowView = false;
        public bool AllowView
        {
            get { return allowView; }
            set { allowView = value; }
        }

        private bool allowAdd = false;
        public bool AllowAdd
        {
            get { return allowAdd; }
            set
            {
                allowAdd = value;
                allowView = allowView || allowAdd;
            }
        }

        private bool allowEdit = false;
        public bool AllowEdit
        {
            get { return allowEdit; }
            set
            {
                allowEdit = value;
                allowView = allowView || allowEdit;
            }
        }

        private bool allowDelete = false;
        public bool AllowDelete
        {
            get { return allowDelete; }
            set
            {
                allowDelete = value;
                allowView = allowView || allowDelete;
            }
        }

        private MemberFunctionEdit memberFunctionEdit;
        public int FunctionOrder
        {
            get
            {
                if (memberFunctionEdit == null) return 0;
                else return memberFunctionEdit.FunctionOrder;
            }
         }
    }
    /// <summary>
    /// the combination of Function and MemberFunction 
    /// </summary>
    public class MemberFunctionEdit:MemberFunction
    {
        public MemberFunctionEdit()
        {
        }

        public MemberFunctionEdit(IDataReader reader)
        {
            this.FromDataReader(reader);
        }

        public override void CopyFrom(object obj)
        {
            base.CopyFrom(obj);

            //functionID = (obj as Function).functionID;
            moduleID = (obj as MemberFunctionEdit).moduleID;
            description = (obj as MemberFunctionEdit).description;
            groupName = (obj as MemberFunctionEdit).groupName;
            canAdd = (obj as MemberFunctionEdit).canAdd;
            canEdit = (obj as MemberFunctionEdit).canEdit;
            canDelete = (obj as MemberFunctionEdit).canDelete;
            functionOrder = (obj as MemberFunctionEdit).functionOrder;

        }
        public override void FromDataReader(IDataReader reader)
        {
            base.FromDataReader(reader);
            if (reader != null && !reader.IsClosed)
            {
                //if (!isNull("FunctionID", reader)) functionID = reader.GetString(reader.GetOrdinal("FunctionID"));
                if (!isNull("ModuleID", reader)) moduleID = reader.GetString(reader.GetOrdinal("ModuleID"));
                if (!isNull("Description", reader)) description = reader.GetString(reader.GetOrdinal("Description"));
                if (!isNull("GroupName", reader)) groupName = reader.GetString(reader.GetOrdinal("GroupName"));
                if (!isNull("CanAdd", reader)) canAdd = reader.GetBoolean(reader.GetOrdinal("CanAdd"));
                if (!isNull("CanEdit", reader)) canEdit = reader.GetBoolean(reader.GetOrdinal("CanEdit"));
                if (!isNull("CanDelete", reader)) canDelete = reader.GetBoolean(reader.GetOrdinal("CanDelete"));
                if (!isNull("FunctionOrder", reader)) functionOrder = reader.GetInt32(reader.GetOrdinal("FunctionOrder"));
            }
        }
        public override void FromDataRow(DataRow row)
        {
            base.FromDataRow(row);

            //if (!row.IsNull("FunctionID")) functionID = (string)row["FunctionID"];
            if (!row.IsNull("ModuleID")) moduleID = (string)row["ModuleID"];
            if (!row.IsNull("Description")) description = (string)row["Description"];
            if (!row.IsNull("GroupName")) groupName = (string)row["GroupName"];
            if (!row.IsNull("CanAdd")) canAdd = (bool)row["CanAdd"];
            if (!row.IsNull("CanEdit")) canEdit = (bool)row["CanEdit"];
            if (!row.IsNull("CanDelete")) canDelete = (bool)row["CanDelete"];
            if (!row.IsNull("FunctionOrder")) functionOrder = (int)row["FunctionOrder"];
        }

        private string moduleID = string.Empty;
        public string ModuleID
        {
            get { return moduleID; }
            set { moduleID = value; }
        }

        private string groupName = string.Empty;
        public string GroupName
        {
            get { return groupName; }
            set { groupName = value; }
        }

        private string description = string.Empty;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        private bool canAdd = false;
        public bool CanAdd
        {
            get { return canAdd; }
            set { canAdd = value; }
        }

        private bool canEdit = false;
        public bool CanEdit
        {
            get { return canEdit; }
            set { canEdit = value; }
        }

        private bool canDelete = false;
        public bool CanDelete
        {
            get { return canDelete; }
            set { canDelete = value; }
        }

        private int functionOrder = 0;
        public int FunctionOrder
        {
            get { return functionOrder; }
            set { functionOrder = value; }
        }
 
    }
}
