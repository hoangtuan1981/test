using System;
using System.Collections.Generic;
using System.Text;

namespace GS.Data
{
    public class Member:UserTracking
    {
        public enum enumMemberType { USER, USERGROUP}
        public Member()
        { }
        public override void CopyFrom(object obj)
        {
            base.CopyFrom(obj);

            memberID = (obj as Member).memberID;
            memberName = (obj as Member).memberName;
            memberType = (obj as Member).memberType;
            description = (obj as Member).description;
            listMemberFunctionEdit = (obj as Member).listMemberFunctionEdit.Clone() as ListBase<MemberFunctionEdit>;
            listPrivilege = (obj as Member).listPrivilege.Clone() as ListBase<MemberFunction>;
            property = this.CloneDictionary((obj as Member).property) as Dictionary<string, string>;
        }
        public Member(System.Data.IDataReader reader)
        {
            FromDataReader(reader);
        }
        public override void FromDataReader(System.Data.IDataReader reader)
        {
            base.FromDataReader(reader);
            if (reader != null && !reader.IsClosed)
            {
                if (!isNull("MemberID", reader)) memberID = reader.GetString(reader.GetOrdinal("MemberID"));
                if (!isNull("MemberName", reader)) memberName = reader.GetString(reader.GetOrdinal("MemberName"));
                if (!isNull("MemberType", reader)) memberType = reader.GetString(reader.GetOrdinal("MemberType"));
                if (!isNull("Description", reader)) description = reader.GetString(reader.GetOrdinal("Description"));
            }
        }

        public override void FromDataRow(System.Data.DataRow row)
        {
            base.FromDataRow(row);
            if (!row.IsNull("MemberID")) memberID = (string)row["MemberID"];
            if (!row.IsNull("MemberName")) memberName = (string)row["MemberName"];
            if (!row.IsNull("MemberType")) memberType = (string)row["MemberType"];
            if (!row.IsNull("Description")) description = (string)row["Description"];
        }

        public void LoadPropertyFromDataRow(System.Data.DataRow row)
        {
            if (!row.IsNull("PropertyValue"))
            {
                property.Add(row["PropertyID"].ToString(), row["PropertyValue"].ToString());
            }
        }
        protected string memberID =string.Empty;
        public string MemberID
        {
            get { return memberID; }
            set { memberID= value; }
        }

        protected string memberName = string.Empty;
        public string MemberName
        {
            get { return memberName; }
            set { memberName = value; }
        }

        protected string memberType = string.Empty;
        public string MemberType
        {
            get { return memberType; }
            set { memberType = value; }
        }

        protected string description = string.Empty;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        protected Dictionary<string, string> property = new Dictionary<string, string>();
        public Dictionary<string, string> Property
        {
            get { return property; }
            set { property = value; }
        }

        private ListBase<MemberFunctionEdit> listMemberFunctionEdit = new ListBase<MemberFunctionEdit>();
        public ListBase<MemberFunctionEdit> ListMemberFunctionEdit
        {
            get { return listMemberFunctionEdit; }
            set { listMemberFunctionEdit = value; }
        }

        private ListBase<MemberFunction> listPrivilege = new ListBase<MemberFunction>();
        /// <summary>
        /// List quyền truy cập chương trình của user
        /// </summary>
        public ListBase<MemberFunction> ListPrivilege
        {
            get { return listPrivilege; }
            set { listPrivilege = value; }
        }
        
    }

    public class User : Member
    {
        public User()
        {
            MemberType = User.enumMemberType.USER.ToString();
        }
        public enum enumPropertyID { PASSWORD, ISADMIN}

        public User(Member member)
        {
            //this.MemberID = member.MemberID;
            //this.MemberName = member.MemberID;
            //this.MemberType = member.MemberType;
            this.CopyFrom(member);
        }
        public string LoginName
        {
            get { return memberID; }
            set { memberID = value; }
        }

        public string UserName
        {
            get { return memberName; }
            set { memberName = value; }
        }

        public string Password
        {
            get
            {
                if (Property.ContainsKey(User.enumPropertyID.PASSWORD.ToString()))
                    return Crypto.DecryptString(Property[User.enumPropertyID.PASSWORD.ToString()]);
                else
                    return string.Empty;
            }
            set
            {
                if (Property.ContainsKey(User.enumPropertyID.PASSWORD.ToString()))
                    Property[User.enumPropertyID.PASSWORD.ToString()] = Crypto.EncryptString(value);
                else
                    Property.Add(User.enumPropertyID.PASSWORD.ToString(), Crypto.EncryptString(value));
            }
        }
        public bool IsAdmin
        {
            get
            {
                if (Property.ContainsKey(User.enumPropertyID.ISADMIN.ToString()))
                    return Boolean.Parse(Property[User.enumPropertyID.ISADMIN.ToString()]);
                else
                    return false;
            }
            set
            {
                if (Property.ContainsKey(User.enumPropertyID.ISADMIN.ToString()))
                    Property[User.enumPropertyID.ISADMIN.ToString()] = value.ToString();
                else
                    Property.Add(User.enumPropertyID.ISADMIN.ToString(), value.ToString());
            }
        }
    }

    public class UserGroup : Member
    {
        public UserGroup()
        {
            MemberType = Member.enumMemberType.USERGROUP.ToString();
        }
        public override void CopyFrom(object obj)
        {
            base.CopyFrom(obj);

            listMember = (obj as UserGroup).listMember.Clone() as ListBase<Member>;
        }
        private ListBase<Member> listMember = new ListBase<Member>();
        public ListBase<Member> ListMember
        {
            get { return listMember; }
            set { listMember = value; }
        }
    }
}
