using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Data.Common;
namespace GS.Data
{
    public class ExcelUtils
    {
        public static DataTable GetDataTable(string excelFile)
        {
            return ExcelUtils.GetDataTable(excelFile,0);
        }
        public static DataTable GetDataTable(string excelFile,int sheetIndex)
        {
            OleDbConnection cn = CreateConnection(excelFile);
            
            cn.Open();
            

            DataTable dt = new DataTable();
            OleDbDataAdapter adp = new OleDbDataAdapter(BuildQuery(cn,sheetIndex), cn);
            adp.Fill(dt);
            cn.Close();
            return dt;
        }

        private static OleDbConnection CreateConnection(string excelFile)
        {
            OleDbConnection cn = new OleDbConnection();
            cn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + excelFile + ";Extended Properties=\"Excel 8.0;IMEX=1;\"";
            return cn;
        }


        public static DbDataReader GetDataReader(string excelFile)
        {
            return ExcelUtils.GetDataReader(excelFile,0);
        }
        public static DbDataReader GetDataReader(string excelFile, int sheetIndex)
        {
            OleDbConnection cn = CreateConnection(excelFile);
            cn.Open();

            OleDbCommand cmd = cn.CreateCommand();
            cmd.CommandText = BuildQuery(cn, sheetIndex);

            return (DbDataReader) cmd.ExecuteReader(CommandBehavior.CloseConnection);
        }

        private static string BuildQuery(OleDbConnection cn, int sheetIndex)
        {
            DataTable schema = cn.GetSchema("Tables");
            string tableName = schema.Rows[sheetIndex]["Table_name"].ToString();
            schema = cn.GetSchema("Columns", new string[] { null, null, tableName, null });
           
            DataView schemaView = schema.DefaultView;
            schemaView.Sort = "ORDINAL_POSITION";
            string sql = "";
            foreach (DataRowView row in schemaView)
            {
                sql += string.Format(",'' & [{0}] as [{0}]",row["Column_name"]);
            }
            sql = sql.Substring(1);
            sql = string.Format("SELECT {0} From [{1}]", sql,tableName);
            return sql;
        }
    }
}
