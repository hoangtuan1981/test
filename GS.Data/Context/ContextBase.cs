using System;
using System.Collections.Generic;
using System.Text;

using GS.Data;

namespace GS.Data
{
    public class ContextBase
    {
        public static User CurrentUser=new User();
        public static string AppName;
        public static string AppVersion;

        public static DateTime WorkingDate = DateTime.Today;
        public static GS.Data.ListBase<MemberFunction> MemberFunctions;
        public static string LanguageFolder = "Vietnam";
        public static string WorkingPeriod = "0810";
    }
}
